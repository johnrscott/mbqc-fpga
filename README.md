# A digital control system for photonic MBQC

This repository contains an implementation of a simplified control system for photonic measurement-based quantum computing (MBQC), targetting Xilinx FPGAs. The design is written in VHDL, and provides a reference for how to perform arbitrary one-qubit gates and CNOT gates in the presence of an ideal cluster state generator.

The inputs to the FPGA are assumed to be the (amplified) output pulses from single photon detectors and the outputs are digital signals which control the adaptive measurement settings which are necessary to perform gates in MBQC. 

The design is intended to provide a testbed for analysing the control requirements for photonic MBQC architectures. It could be extended in several directions, such as:

* How to perform the percolation algorithms that are likely necessary when using non-ideal cluster state generators
* How to choose measurement patterns that are photonic-MBQC-friendly, such that they lessen the requirements on the control system.

A preprint discussing the results in this repository is [here](https://arxiv.org/abs/2109.04792).

**Visit the documentation [here](https://johnrscott.gitlab.io/mbqc-fpga/index.html).**


## The FPGA design

The FPGA design is contained in the `design/` subdirectory. It is a Vivado project, called `mbqc.xpr`. To install Xilinx Vivado, open the [download page](https://www.xilinx.com/support/download.html) and download and install Vivado 2020.2 (or more recent -- the project has been developed using Vivado 2020.2 and Linux). Once Vivado is installed, open the project by navigating to the top level directory and running

```bash
vivado design/mbqc.xpr
```

The design contains two top-level block designs, `mbqc1` (single-qubit) and `mbqc2` (multi-qubit). The design is written using VHDL, and can be synthesized for any FPGA in the free Webpack (or any other FPGA you have a license for). When synthesizing `mbqc1`, use the contraints file `single_qubit.xdc`, and when synthesizing `mbqc2`, use `multi_qubit.xdc`. You can modify the properties of input/output ports, and set input/output delay constraints by modifying these files.

### Timing analysis

The results of the static timing analysis for the design is contained in the `results/` folder. Each subdirectory contains a readme explaining how the analysis was performed.

## MBQC Simulator

The repository also contains an MBQC simulator, contained in the `simulator/` directory, that is used for verifying the functional correctness of the FPGA design. The progam outputs data which is used as the input to VHDL testbenches.

The simplest use of the program is as folows:

```bash
mbqcsim -c circuit.txt -o results
```

where the square brackets indicate that any of those arguments are optional. The `-c` option specifies a circuit file, which has the following format:

```
N=3
u 0 0.1 0.2 0.3
cnot 0 1
u 1 -0.3 -0.2 -0.1
cnot 1 2
u 2 0 0 1
```

The first line specifies the number of logical qubit rows. The `cnot c t` line adds a CNOT gate between the logical qubits indexed `c` and `t`. The `u t ax0 az1 ax2` adds an arbitrary one-qubit gate to logical qubit `t`, specified by Euler angles `ax0` (first X rotation), `az1` (subsequent Z rotation) and `ax2` (another X rotation). Gates are compiled into a measurement pattern by inserting one-qubit gates at the earliest available measurement round for a given logical qubit, and then padding logical qubits with identity patterns whenever a CNOT is required.

After running the program, the output of the program will be a file `results.log` containing the simulation results from each measurement round. The file contains the measurement results from each measurement round for each logical qubit; the program used for each measurement round; and the resulting adaptive measuremetn settings and byproduct operators for each measurement round. 

The main purpose of the program is to produce testbench input files for verifying the FPGA design. To produce these files, run

```bash
mbqcsim -c circuit.txt -o results -p prog -s stb -m mtb
```

This will result in `stb*.csv` files for the single-qubit testbenches and an `mtb.csv` file for the multi-qubit testbench. In addition, the programs for each logical qubit are written to `prog*.coe`.

Before compiling the program, install `g++-10`, `cmake` (at least version 3.12), and the prerequisite quantum simulator [QSL](https://github.com/lanamineh/qsl). 

To compile the program, navigate to the `simulator/` directory and run the following commands

```bash
mkdir build
cd build
cmake ..
cmake --build .
```

The program `mbqcsim` will be available in the `build/bin` directory.

### Documentation for the FPGA design and the simulator

The FPGA design and MBQC simulator are documented using inline doxygen comments. This documentation can be generated from the top level directory by running

```bash
make docs
```

You can view the documentation by running

```bash
firefox docs/html/index.html # Or whatever browser you prefer
```

The documentation is also available [here](https://johnrscott.gitlab.io/mbqc-fpga/index.html).
