set period [ get_property PERIOD [get_clocks clk_p_in] ]

# Setup/hold times for the adaptive measurement output s
# set s_setup [ expr $period / 6 ]
set s_setup 0
set s_hold [ expr $period / 6 ]

# Setup/hold times for the control signals (reset, enable)
set ctrl_tco_min [ expr - $period / 6 ]
set ctrl_tco_max [ expr - $period / 6 ]

# Setup/hold times for the measurement input
set meas_tco_min 0
set meas_tco_max 0

set_input_delay -clock [get_clocks clk_p_in] -min -add_delay $ctrl_tco_min [get_ports enable]
set_input_delay -clock [get_clocks clk_p_in] -max -add_delay $ctrl_tco_max [get_ports enable]

set_input_delay -clock [get_clocks clk_p_in] -min -add_delay $ctrl_tco_min [get_ports reset]
set_input_delay -clock [get_clocks clk_p_in] -max -add_delay $ctrl_tco_max [get_ports reset]

set_output_delay -clock [get_clocks clk_p_in] -min -add_delay -$s_hold [get_ports {ops[*]}]
set_output_delay -clock [get_clocks clk_p_in] -max -add_delay $s_setup [get_ports {ops[*]}]

set_output_delay -clock [get_clocks clk_p_in] -min -add_delay -$s_hold [get_ports s[*]]
set_output_delay -clock [get_clocks clk_p_in] -max -add_delay $s_setup [get_ports s[*]]

set_input_delay -clock [get_clocks clk_p_in] -min -add_delay -$meas_tco_min [get_ports meas[*]]
set_input_delay -clock [get_clocks clk_p_in] -max -add_delay $meas_tco_max [get_ports meas[*]]

#set_property IOB TRUE [get_ports s]
set_property IOB TRUE [get_ports meas[*]]
#set_property IOB TRUE [get_ports ops[*]]

# Set port properties
set_property IOSTANDARD HSTL_I [get_ports clk_p_in]
set_property IOSTANDARD HSTL_I [get_ports locked]
set_property IOSTANDARD HSTL_I [get_ports meas[*]]
set_property IOSTANDARD HSTL_I [get_ports enable]
set_property IOSTANDARD HSTL_I [get_ports reset]
set_property IOSTANDARD HSTL_I [get_ports s[*]]
set_property IOSTANDARD HSTL_I [get_ports ops[*]]

# Set output port slew rate to fast
set_property SLEW FAST [get_ports s[*]]
set_property SLEW FAST [get_ports ops[*]]

