--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file latch.vhd
--! \brief Latch module


library ieee;
use ieee.std_logic_1164.all;

Library unisim;
use unisim.vcomponents.all;


--! \brief Latch module
--!
--! This module will convert a pulse
--! at set into a steady high level at
--! value, until the reset signal is
--! asserted. 
--!
--! It is important that the set process
--! is asynchronous and the reset process
--! is synchronous, because the latch
--! might need to be set again before
--! the reset input has fallen back to
--! zero. The reset input is intended
--! to be used with clk_r.
--!
--! \param set When the set input is 
--! asserted, the output is asychronously
--! set high, even if the reset input
--! is also high. Repeated assertions of set
--! have no effect.
--!
--! \param reset On a rising edge of the
--! reset pin, if set is not currently
--! high, the output will be pulled low.
--! Since the set input is assumed to be
--! a very short pulse, a rising edge of
--! reset should always reset the module
--!
--! \param q The output from the module
--! 
--!
entity latch is
port (
    set : in std_logic;
    clk_s, clk_r : in std_logic;
    q : out std_logic
);
end latch;

--!
--! \brief Implementation of latch
--!
--! This seems like quite a simple module
--! to implement, but getting the behaviour
--! right turned out to be a bit tricky. The
--! key point is that the module needs to be
--! settable even when reset is high, which 
--! means a simple S/R flip flop will not do.
--!
architecture behav_fdpe of latch is

    signal latch_out: std_Logic;
    signal clear_signal: std_logic;

begin

--    -- Concurrent asignment of latch_out to q
--    q <= latch_out;
    
--    -- Generate the clear signal from the clock and
--    -- the output
--    clear_signal <= clk_s and clk_r;

----    proc_trigger: process(set, clear_signal) is
----    begin
    
----        if clear_signal = '1'
----        then
----            q <= '0';
----        elsif rising_edge(set)
----        then
----            q <= '1';
----        end if;
----    end process proc_trigger;

--    -- LDCE: Transparent latch with Asynchronous Reset and
--    -- Gate Enable.
--    -- 7 Series
--    -- Xilinx HDL Libraries Guide, version 2012.2
--    LDCE_inst : LDCE
--    generic map (
--        INIT => '0' -- Initial value of latch ('0' or '1')
--    ) 
--    port map (
--        Q => latch_out, -- Data output
--        CLR => clear_signal, -- Asynchronous clear/reset input
--        D => set, -- Data input
--        G => '1', -- Gate input 
--        GE => '1' -- Gate enable input
--    );

    proc1: process (set,clk_r) is
    begin
        -- Asynchronous set process
        if set = '1'
        then
            q <= '1';
        -- Synchronous reset process
        elsif rising_edge(clk_r)
        then
            q <= '0';
        end if;
    end process;

end behav_fdpe;

architecture behav_ldce of latch is

    signal latch_out: std_Logic;
    signal clear_signal: std_logic;

begin

    -- Concurrent asignment of latch_out to q
    q <= latch_out;
    
    -- Generate the clear signal from the clock and
    -- the output
    clear_signal <= clk_s and clk_r;

--    proc_trigger: process(set, clear_signal) is
--    begin
    
--        if clear_signal = '1'
--        then
--            q <= '0';
--        elsif rising_edge(set)
--        then
--            q <= '1';
--        end if;
--    end process proc_trigger;

    -- LDCE: Transparent latch with Asynchronous Reset and
    -- Gate Enable.
    -- 7 Series
    -- Xilinx HDL Libraries Guide, version 2012.2
    LDCE_inst : LDCE
    generic map (
        INIT => '0' -- Initial value of latch ('0' or '1')
    ) 
    port map (
        Q => latch_out, -- Data output
        CLR => clear_signal, -- Asynchronous clear/reset input
        D => set, -- Data input
        G => '1', -- Gate input 
        GE => '1' -- Gate enable input
    );

--    proc1: process (set,clk_r) is
--    begin
--        -- Asynchronous set process
--        if set = '1'
--        then
--            q <= '1';
--        -- Synchronous reset process
--        elsif rising_edge(clk_r)
--        then
--            q <= '0';
--        end if;
--    end process;

end behav_ldce;