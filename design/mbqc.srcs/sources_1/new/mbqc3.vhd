--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file mbqc3.vhd
--! \brief 3-qubit MBQC system

library ieee;
use ieee.std_logic_1164.all;

--! \brief 3-qubit MBQC system
--!
--! This module is the full MBQC system for
--! three logical qubits. For each logical
--! qubit, it contains a measurement latch,
--! an adaptive measurement setting calculator,
--! and a byproduct system for tracking the
--! byproduct operators. It takes clock
--! inputs, photon detector inputs, and
--! program inputs. It contains 
--!
entity mbqc3 is
port(
    clk_p, clk_s, clk_r, enable, reset : in std_logic;
    p0: in std_logic_vector(14 downto 0); -- for qubit 0
    p1: in std_logic_vector(14 downto 0); -- for qubit 1
    p2: in std_logic_vector(14 downto 0); -- for qubit 2
    m: in std_logic_vector(1 downto 0);
    s: out std_logic_vector(2 downto 0);
    ops0, ops1, ops2: out std_logic_vector(1 downto 0)
);
end mbqc3;

architecture behaviour of mbqc3 is

    component adapt is
    port ( 
        clk_p, clk_s, clk_r, input, enable, reset : in std_logic;
        p: in std_logic_vector(4 downto 0);
        ops_stored: in std_logic_vector(1 downto 0);
        s: out std_logic
    );
    end component adapt;
    
    component byproduct is
    port ( 
        clk_p, clk_s, clk_r, reset, enable : in std_logic;
        p : in std_logic_vector (9 downto 0);
        m : in std_logic_vector (2 downto 0);
        ops_above, ops_below : in std_logic_vector (1 downto 0);
        ops_current, ops_stored : out std_logic_vector (1 downto 0)
    );
    end component byproduct;

    component latch is
    port (
        set : in STD_LOGIC;
        reset : in STD_LOGIC;
        q : out STD_LOGIC
    );
    end component;
    
    -- Bus of measurement latch values
    signal latch_values: std_logic_vector(2 downto 0);

    -- Program for adaptive measurement settings
    signal s_prog_0 : std_logic_vector(4 downto 0);
    signal s_prog_1 : std_logic_vector(4 downto 0);
    signal s_prog_2 : std_logic_vector(4 downto 0);
    
    -- Program for byproduct operator systems
    signal b_prog_0 : std_logic_vector(9 downto 0);
    signal b_prog_1 : std_logic_vector(9 downto 0);
    signal b_prog_2 : std_logic_vector(9 downto 0);
    
    -- Byproduct operator buses
    signal ops_current_0, ops_current_1, ops_current_2 
    : std_logic_vector(1 downto 0);
 
    signal ops_stored_0, ops_stored_1, ops_stored_2 
    : std_logic_vector(1 downto 0);
    
    -- Measurement inputs for byproduct systems
    signal m0, m1, m2: std_logic_vector(2 downto 0);

begin

    -- Concurrent assign programs
    s_prog_0 <= p0(14 downto 10);
    s_prog_1 <= p1(14 downto 10);
    s_prog_2 <= p2(14 downto 10);    
    b_prog_0 <= p0(9 downto 0);
    b_prog_1 <= p1(9 downto 0);
    b_prog_2 <= p2(9 downto 0);   

    -- Concurrent assign byproduct out
    ops0 <= ops_current_0;
    ops1 <= ops_current_1;
    ops2 <= ops_current_2;
    
    --! Latch for qubit 0
    latch0: latch port map (
        set => m(0),
        reset => clk_r,
        q => latch_values(0)
    );
    
    --! Latch for qubit 1
    latch1: latch port map (
        set => m(1),
        reset => clk_r,
        q => latch_values(1)
    );
    
    --! Latch for qubit 2
    latch2: latch port map (
        set => m(0),
        reset => clk_r,
        q => latch_values(2)
    );
    
    --! Adaptive system for qubit 0
    adapt0 : adapt port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r,
        reset => reset,
        enable => enable,
        p => s_prog_0,
        input => latch_values(0),
        ops_stored => ops_stored_0,
        s => s(0)
    );
    
    --! Adaptive system for qubit 1
    adapt1 : adapt port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r,
        reset => reset,
        enable => enable,
        p => s_prog_1,
        input => latch_values(1),
        ops_stored => ops_stored_1,
        s => s(1)
    );

    --! Adaptive system for qubit 2
    adapt2 : adapt port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r,
        reset => reset,
        enable => enable,
        p => s_prog_2,
        input => latch_values(2),
        ops_stored => ops_stored_2,
        s => s(2)
    );
    
    -- Byproduct system for qubit 0
    m0 <= '0' & latch_values(0) & latch_values(1);
    byp0 : byproduct port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r,
        reset => reset,
        enable => enable,
        p => b_prog_0,
        m => m0,
        ops_above => "00", -- No connection above
        ops_below => ops_current_1, -- Connect to qubit 1
        ops_current => ops_current_0,
        ops_stored => ops_stored_0
    );
    
    -- Byproduct system for qubit 1
    m1 <= latch_values(0) & latch_values(1) & latch_values(2);
    byp1 : byproduct port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r,
        reset => reset,
        enable => enable,
        p => b_prog_1,
        m => m1,
        ops_above => ops_current_0, -- Connect to qubit 0
        ops_below => ops_current_2, -- Connect to qubit 2
        ops_current => ops_current_1,
        ops_stored => ops_stored_1
    );
    
    -- Byproduct system for qubit 2
    m2 <= latch_values(1) & latch_values(2) & '0';
    byp2 : byproduct port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r,
        reset => reset,
        enable => enable,
        p => b_prog_2,
        m => m2,
        ops_above => ops_current_0, -- Connect to qubit 1
        ops_below => "00", -- no connection below
        ops_current => ops_current_2,
        ops_stored => ops_stored_2
    );

end;