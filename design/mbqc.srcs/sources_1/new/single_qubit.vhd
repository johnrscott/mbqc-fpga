--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file single_qubit.vhd
--! \brief Single Logical qubit module

library ieee;
use ieee.std_logic_1164.all;

--! \brief Single Logical qubit module
entity single_qubit is
port ( 
    clk_p, clk_s, clk_r, enable, reset : in std_logic;
    meas: in std_logic;
    prog: in std_logic_vector(15 downto 0);
    ops: out std_logic_vector(1 downto 0);
    s: out std_logic
);
end single_qubit;

--! 
--! \brief Implementation of qubit moduel
--!
--! This module can be used for testing the
--! behaviour of the single logical qubit. The
--! measurements from the logical qubits above
--! and below are hardwired to zero, so it is 
--! only possible to test single qubit gates.
--!
architecture behav of single_qubit is

    component latch is
    port (
        set : in std_logic;
        clk_s, clk_r : in std_logic;
        q : out std_logic
    );
    end component latch;

    component qubit is
    port ( 
        clk_p, clk_s, clk_r, enable, reset : in std_logic;
        m_0, m_1, m_2: in std_logic;
        prog: in std_logic_vector(15 downto 0);
        ops_above, ops_below: in std_logic_vector(1 downto 0);
        ops: out std_logic_vector(1 downto 0);
        s: out std_logic
    );
    end component qubit;
    
    -- Internal measurement latch outputs
    signal m: std_logic;
    
    -- Configuration specifications
    for all: latch use entity work.latch(behav_ldce);
    for all: qubit  use entity work.qubit(behav);
begin

    -- Latch instane
    latch_0: latch port map (
        set => meas,
        clk_s => clk_s,
        clk_r => clk_r,
        q => m
    );

    -- Single qubit
    qubit_0: qubit port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r, 
        enable => enable, 
        reset => reset,
        m_2 => '0',
        m_1 => m,
        m_0 => '0',
        prog => prog(15 downto 0),
        ops_below => "00",
        ops => ops,
        ops_above=> "00",
        s => s
    );

end behav;
