--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file byproduct.vhd
--! \brief Byproduct operator subsystem

library ieee;
use ieee.std_logic_1164.all;

--! \brief Byproduct operator system
--!
--! This module is responsible for keeping track 
--! of the byproduct operators (ops) for the 
--! current logical qubit. The operators are stored
--! in a two bit wide register ops [1:0] = 0bxz. 
--!
--! The module takes the three clock inputs clk_p, 
--! clk_s and clk_r. It is assumed that the inputs
--! become valid on the rising edge of clk_p or before.
--!
--! On clk_s, the internal byproduct operators are 
--! updated according to the measurement inputs m 
--! and the program p. 
--!
--! On clk_r, at the boundary of a logical gate, 
--! the commutation correction for the operators is
--! performed. For a CNOT gate, this means the 
--! byproduct operators are modified using ops_above 
--! or ops_below, which come from the neighbouring 
--! logical qubits. In the case of a single qubit gate,
--! the internal byproduct operators are copied to 
--! the ops_stored output which is used for 
--! measurement settings throughout the next logical 
--! gate. The commutation correction and storing of 
--! the byproduct operators happens on the last 
--! measurement round of the previous gate, ready 
--! for the first measurement round of the next gate.
--! The c_prog input control this part of the 
--! module.
--!
--! It is also sometimes necessary to add constants
--! to the byproduct operators. This is controlled
--! by the commutation correction module.
--!
--! There are two outputs containing byproduct 
--! operators. The current byproduct operators are 
--! stored in ops, which is updated after each
--! round of measurements. The stored byproduct 
--! operators ops_stored are only updated at the 
--! end of each gate, and are used for measurement 
--! settings. 
--!
--! If reset is high on the rising edge of clk_s, 
--! then ops_current is reset. If it is high on 
--! the rising edge of clk_r, then ops_stored is
--! reset. The enable input is used as the clock 
--! enable for both the synchronous clk_s and clk_r 
--! operations. If enable is low, then neither 
--! ops_current nor ops_stored will be updated on 
--! clk_s or clk_r respectively.
--!
--! \param b_prog The program input that controls
--! how the current byproduct operators are computed
--! from the measurement inputs. The low 3 bits are 
--! used as a mask with the measurements for the
--! current and neighbouring qubits for calculating 
--! the z part of the byproduct operator. The high 3 
--! bits are used to calculate the z part.
--!
--! \param c_prog The program input that controls
--! how the commutation correction is performed,
--! adds constants to the byproduct operators, and
--! controls the storing of the byproduct operators.
--! Bit 0 high indicates that the byproduct operators
--! should be stored. For the meaning of the other
--! bits, see the comm_correct module documentation. 
--! b_prog[6] indicates
--!
--! \param m The outcomes from the last round of 
--! measurements. This is a set of three inputs,
--! one from the current qubit m[1], one from the qubit
--! above m[2], and one from the qubit below m[0].
--! It is very important that these are connected
--! directly to the measurement latches, so that
--! the data is valid on the rising edge of clk_s.
--! It must not be the output from the shift 
--! registers in the adaptive module, because these
--! are not valid until the rising edge clk_s.
--!
--! \param ops The current value of the 
--! byproduct operators from this module
--! When connecting this module to
--! the surrounding byproduct operator
--! modules, it is important to use this
--! output as the input to ops_above
--! and ops_below. Do not use ops_stored.
--! The only use for ops_stored is as 
--! the input to the adaptive measurement
--! settings. ops_stored will only be
--! updated if the next gate is a single
--! qubit gate, so it may become very 
--! out of date if there is a string of
--! CNOTs.
--!
--!
entity byproduct is
port ( 
    clk_p, clk_s, clk_r, reset, enable : in std_logic;
    b_prog : in std_logic_vector (5 downto 0);
    c_prog: in std_logic_vector (4 downto 0);
    m : in std_logic_vector (2 downto 0);
    ops_above, ops_below : in std_logic_vector (1 downto 0);
    ops_current, ops_stored : out std_logic_vector (1 downto 0)
);
end byproduct;

--!
--! \brief Implementation of byproduct module
--!
architecture behaviour of byproduct is

    --! The CNOT commutation correction  module
    component comm_correct is
    port (
        clk_r: in std_logic;
        c_prog: in std_logic_vector(4 downto 0);
        ops_above, ops_below : in std_logic_vector (1 downto 0);
        comm: out std_logic_vector (1 downto 0)
    );
    end component comm_correct;


    component ops_update is
    port (
        p: in std_logic_vector (5 downto 0);
        m: in std_logic_vector (2 downto 0);
        update : out std_logic_vector (1 downto 0));
    end component ops_update;

    --! Internal register for storing the
    --! current byproduct operators
    signal ops : std_logic_vector(1 downto 0);
    
    --! Register for the commutation correction.
    --! Since this is computed at clk_r, it cannot
    --! be added directly to ops since that register
    --! is clocked on clk_s. Instead, it is stored 
    --! here and added to ops on the rising edge of
    --! clk_s.
    --!
    --! It is important that this register is reset
    --! to zero when no commutation correction is 
    --! needed, because it is always added to the
    --! byproduct operators on clk_s.
    --!
    signal comm : std_logic_vector(1 downto 0);
    
    --! This register contains the update to the 
    --! current byproduct operators. It is calculated
    --! combinationally from the program p and the 
    --! measurement outcomes m
    --!
    signal update: std_logic_vector(1 downto 0);

begin
    
    -- Instantiate the CNOT commutation 
    -- correction module
    cc1: comm_correct port map (
        clk_r => clk_r,
        c_prog => c_prog,
        ops_above => ops_above,
        ops_below => ops_below,
        comm => comm
    );
    
    ou1: ops_update port map (
        p => b_prog,
        m => m,
        update => update
    );

    -- Concurrent assignment of the internal
    -- ops register to the output ops_current
    ops_current <= ops;

    --! On the rising edge of clk_s
    --! the internal byproduct 
    --! operators must be updated with
    --! the outcomes of the last round 
    --! of measurement results from
    --! the current and neighbouring
    --! qubits.
    proc_s: process (clk_s) is
    begin
        if clk_s'event and clk_s = '1'
        then
            if reset = '1'
            then
                ops <= "00";
            elsif enable = '1'
            then
                -- Update x according to the program
                ops(0) <= ops(0) xor comm(0) xor update(0);
                -- Update z according to the program
                ops(1) <= ops(1) xor comm(1) xor update(1);
            end if;
        end if;
    end process proc_s;
    
    --! On the rising edge of clk_r, it is
    --! necessary to store a copy of the 
    --! current byproduct operators if the
    --! next measurement round is the first
    --! gate on a one-qubit gate.
    --! 
    proc_r: process (clk_r) is
    begin
        if clk_r'event and clk_r = '1'
        then 
            if reset = '1'
            then
                ops_stored <= "00";
            elsif c_prog(0) = '1'
            then
                ops_stored <= ops;
            end if;
        end if;
    end process proc_r;
    
end architecture behaviour;


