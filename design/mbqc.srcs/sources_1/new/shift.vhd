--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file shift.vhd
--! \brief Shift register module

library ieee ;
use ieee.std_logic_1164.all;

--!
--! \brief Shift register module
--!
--! This module is a shift register that takes
--! a single serial input and provides a 
--! parallel output that stores the last three
--! values at the input. Shifting only happens
--! when the enable input is high. and the 
--! module can be synchronously reset by pulling
--! the reset input high on a rising edge of
--! the clock.
--!
--! The parallel output shifts from left to right.
--! The most significant bit is populated with
--! the latest value of the input.
--!
--! The module should be used to store the most
--! recent measurement outcomes for use in 
--! calculating the next measurement setting. 
--!
--! \param clock The state of the shift register 
--! only changes on a rising edge of this clock input.
--!
--! \param input The input to the shift register.
--! On the rising edge of the clock input, the
--! data on this input is shifted to the most
--! significant bit of the output
--!
--! \param enable The enable pin. Data is only
--! shifted into the register on a rising 
--! clock edge if the enable input is high.
--!
--! \param The synchronous reset pin. If the 
--! reset pin is high on a rising clock edge 
--! then the output is reset to zero.
--!
--!
entity shift_reg is
port(
    input, clock, enable, reset: in std_logic;
	output: out std_logic_vector(2 downto 0)
);
end shift_reg;

--! 
--! \brief Implementation of shift_reg module
--!
--! This implementation uses an internal 
--! register to store the shift register. An
--! improvement would be to make the length
--! of the shift register a generic parameter.
--! 
--! 
architecture behv of shift_reg is

    -- initialize the declared signal
    signal S: std_logic_vector(2 downto 0):="000";

begin
    
    process(input, clock, enable, S)
    begin

	-- everything happens upon the clock changing
	if clock'event and clock='1' then
	   if reset = '1'
	   then
	       S <= "000";
	   elsif enable = '1' 
	   then
	       S <= input & S(2 downto 1);
	   end if;
	end if;

    end process;
	
    -- concurrent assignment
    output <= S;

end behv;

----------------------------------------------------