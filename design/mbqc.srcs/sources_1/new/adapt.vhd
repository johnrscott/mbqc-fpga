--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file adapt.vhd
--! \brief Adaptive measurement settings module

library ieee;
use ieee.std_logic_1164.all;

--! \brief Adaptive measurement setting module
--!
--! This module is responsible for generating the
--! measurement setting s that must be used in the
--! next round of measurements. The setting s is
--! constructed by forming the xor of a selection 
--! of past measurements results and the stored
--! byproduct operators from the current qubit.
--! 
--! The program p is responsible for deciding which
--! bits to xor together. The low three bits p[2:0]
--! determine which of the past three measurement
--! results in include in the xor, where p[2] is the
--! most recent measurement result. p[4;3] is 
--! responsible for indicating which of the stored 
--! byproduct operators should be included: p[4] 
--! is for x and p[3] is for z
--!
--! The output s is formed combinationally from the 
--! byproduct operators and the shift register output 
--! using a mask which is equal to the program p. 
--! It is necessary to copy p because the value of s
--! must persist until the next rising edge clk_s, 
--! but p will become invalid at the rising edge of clk_p
--!
--! For a similar reason, it is necessary to store a 
--! copy of the byproduct operators in the mask register.
--! This is because the stored byproduct operators are 
--! updated on the rising edge of clk_r on the last qubit
--! of the gate, which will invalidate s before it is
--! required on the rising edge of clk_p (the case which
--! causes this problem is when two single qubit gates
--! are placed end to end).
--!
--! It is not possible to calculate the value of s on
--! the rising edge of clk_s, because data from the 
--! output of the shift register is not valid yet.
--! 
entity adapt is
port ( 
    clk_p, clk_s, clk_r, input, enable, reset : in std_logic;
    p: in std_logic_vector(4 downto 0);
    ops_stored: in std_logic_vector(1 downto 0);
    s: out std_logic
);
end adapt;

--! 
--! \brief Implementation of adapt module
--!
architecture behavioural of adapt is

    component shift_reg
    port(
        input, clock, enable, reset: in std_logic;
	    output: out std_logic_vector(2 downto 0)
    );
    end component;
    
    --! The mask is used to generate the right
    --! xor combination of byproduct operators
    --! and past measurement outcomes to generate
    --! the new measurement setting. It is a copy
    --! of the program p. It is not possible to 
    --! use p directly because p will change
    --! on the rising edge clk_p, and s must
    --! persist at least until the next rising 
    --! edge clk_s
    signal mask: STD_LOGIC_VECTOR(2 downto 0);
    
    --! Store the term that originates from the
    --! stored byproduct operators. This can be
    --! computed from the program and the stored
    --! byproduct operators on the rising edge of 
    --! clk_s.
    signal byp: STD_LOGIC;

    --! Store the last three measurement outcomes
    signal sr_out: STD_LOGIC_VECTOR(2 downto 0);

begin

    --! Shift register for storing measurement
    --! outcomes
    sr1: shift_reg port map (
        input => input,
        clock => clk_s,
        enable => enable,
        reset => reset,
        output => sr_out
    );
    
    -- Concurrent process on rising edge of clk_s 
    -- to shift data into the shift register. Note
    -- that it is not valid to calculate s here,
    -- even though that would appear simpler, because
    -- the output of the shift register is not valid
    -- until after the rising edge of clk_s. Instead,
    -- the means to generate s is stored in registers
    -- and then used in a combinational calculation
    -- later.
    proc_s: process(clk_s) is
    begin
        if rising_edge(clk_s)
        then
            -- Store a copy of the program as the mask
            -- Incorporate the stored byproduct operators
            -- as a precalculation 
            mask <= p(2 downto 0);
            
            -- Compute the contribution due to the byproduct
            -- operators. This is valid because ops_stored
            -- and p were both set before the rising
            -- edge clk_s
            byp <= (p(4) and ops_stored(1))
                xor (p(3) and ops_stored(0));
        end if;
    end process proc_s;
    
    -- Concurrent assignment of the output s
    -- from the mask and the current state of
    -- the shift register and byproduct
    -- operators
    s <= (mask(0) and sr_out(0))
        xor (mask(1) and sr_out(1))
        xor (mask(2) and sr_out(2))
        xor byp;
        
end behavioural;