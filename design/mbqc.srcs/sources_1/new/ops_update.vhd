--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file ops_update.vhd
--! \brief Simple combinational module for byproduct updates

library ieee;
use ieee.std_logic_1164.all;

--!
--! \brief Byproduct operator update logic
--!
--! This is a simple combinational module
--! containing the xor logic for the byproduct
--! operator update based on the current program 
--! and current measurement values.
--!
--! \param m The current measurement latch values.
--! m[0] is the measuremenet result from the qubit
--! below, m[1] is from the current qubit, and
--! m[2] is from the qubit above.
--!
--! \param p The mask which selects which of the
--! measurement values is used to update which 
--! byproduct operator bit. The low bits p[2:0]
--! are used to select which of m[2:0] are 
--! added modulo-2 to x, and the high bits
--! p[5:3] determine which of m[2:0] are added
--! to x.
--!
entity ops_update is
port (
    p: in std_logic_vector (5 downto 0);
    m: in std_logic_vector (2 downto 0);
    update : out std_logic_vector (1 downto 0));
end ops_update;

architecture behavioural of ops_update is
begin

    -- Update z according to the program
    update(0) <= (m(0) and p(0))
        xor (m(1) and p(1)) 
        xor (m(2) and p(2));
    -- Update x according to the program
    update(1) <= (m(0) and p(3)) 
        xor (m(1) and p(4)) 
        xor (m(2) and p(5));

end behavioural;
