--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file functions.vhd
--! \brief Contains useful functions in a package
--! 

library ieee;
use ieee.std_logic_1164.all;

-- Package Declaration Section
package example_package is

    --!
    --! \brief A function to replace to_hstring in VHDL 2008
    --!
    function to_hstring(slv: std_logic_vector)
        return string;
    
end package example_package;


-- Package Body Section
package body example_package is

    function to_hstring(slv: std_logic_vector) return string is
        constant hexlen : integer := (slv'length+3)/4;
        variable longslv : std_logic_vector(slv'length+3 downto 0) := (others => '0');
        variable hex : string(1 to hexlen);
        variable fourbit : std_logic_vector(3 downto 0);
    begin
        longslv(slv'length-1 downto 0) := slv;
        for i in hexlen-1 downto 0 loop
            fourbit := longslv(i*4+3 downto i*4);
            case fourbit is
                when "0000" => hex(hexlen-i) := '0';
                when "0001" => hex(hexlen-i) := '1';
                when "0010" => hex(hexlen-i) := '2';
                when "0011" => hex(hexlen-i) := '3';
                when "0100" => hex(hexlen-i) := '4';
                when "0101" => hex(hexlen-i) := '5';
                when "0110" => hex(hexlen-i) := '6';
                when "0111" => hex(hexlen-i) := '7';
                when "1000" => hex(hexlen-i) := '8';
                when "1001" => hex(hexlen-i) := '9';
                when "1010" => hex(hexlen-i) := 'A';
                when "1011" => hex(hexlen-i) := 'B';
                when "1100" => hex(hexlen-i) := 'C';
                when "1101" => hex(hexlen-i) := 'D';
                when "1110" => hex(hexlen-i) := 'E';
                when "1111" => hex(hexlen-i) := 'F';
                when "ZZZZ" => hex(hexlen-i) := 'Z';
                when "UUUU" => hex(hexlen-i) := 'U';
                when "XXXX" => hex(hexlen-i) := 'X';
                when others => hex(hexlen-i) := '?';
            end case;
        end loop;
        return hex;
    end function to_hstring;
    
end package body example_package;