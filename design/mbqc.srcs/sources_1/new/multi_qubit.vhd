--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file multi_qubit.vhd
--! \brief Multiple Logical qubit module

library ieee;
use ieee.std_logic_1164.all;

--! \brief Multiple Logical qubit module
--!
--! The generic N is the number of logical qubits
--! (must be at least 2).
--!
--! \param clk_p The photon clock
--! \param clk_s The sample clock
--! \param clk_r The reset clock
--! \param enable Assert this input to begin the
--! measurement pattern. The module is expecting
--! the input to be asserted in time for the rising
--! edge of clk_p, when the first measurement round
--! begins.
--! \param reset A synchronous reset that should be
--! asserted after the clocks become valid, and
--! deasserted before the enable input is set. It
--! initialises the byproduct operators and measurement
--! shift registers to zero.
--! \param meas The photon measurement outcomes, as
--! as std_logic_vector. The nth element is the
--! outcome that corresponds to qubit n.
--! \param prog The program for all N qubits. The
--! nth two-byte word is the program for the nth
--! qubit. This input should be modified on the rising
--! edge of clk_p. 
--! \param ops The byproduct operators for each qubit.
--! The nth pair of bits is for the nth logical qubit
--! \param s The adaptive measurement setting output.
--! The nth setting is for the nth qubit.
--! 
entity multi_qubit is
generic (
    N: integer := 10
);
port ( 
    clk_p, clk_s, clk_r, enable, reset : in std_logic;
    meas: in std_logic_vector(N-1 downto 0);
    prog: in std_logic_vector(16*N-1 downto 0);
    ops: out std_logic_vector(2*N-1 downto 0);
    s: out std_logic_vector(N-1 downto 0)
);
end multi_qubit;

--! 
--! \brief Implementation of qubit module
--!
--! The multiple qubit module is made from multiple
--! copies of the qubit module, with connections to
--! the surrounding qubit modules to transfer the
--! current values of the byproduct operators.
--!
--! The is also an array of latches which take photon
--! measurements and pass the latched results to the
--! correct qubit modules.
--!
architecture behav of multi_qubit is

    component latch is
    port (
        set : in std_logic;
        clk_s, clk_r : in std_logic;
        q : out std_logic
    );
    end component latch;

    component qubit is
    port ( 
        clk_p, clk_s, clk_r, enable, reset : in std_logic;
        m_0, m_1, m_2: in std_logic;
        prog: in std_logic_vector(15 downto 0);
        ops_above, ops_below: in std_logic_vector(1 downto 0);
        ops: out std_logic_vector(1 downto 0);
        s: out std_logic
    );
    end component qubit;

    -- The byproduct operators for all the logical qubits
    signal ops_internal: std_logic_vector(2*N-1 downto 0);
    
    -- Internal measurement latch outputs
    signal m: std_logic_vector(N-1 downto 0);
    
    -- Configuration specifications
    for all : latch use entity work.latch(behav_ldce);
    for all: qubit  use entity work.qubit(behav);

begin

    -- Concurrent assignment of byproduct operators to output
    ops <= ops_internal;

    -- Generate an array of measurement latches
    gen_latches: for i in 0 to N-1 generate
        latch_i : latch port map (
            set => meas(i),
            clk_s => clk_s,
            clk_r => clk_r,
            q => m(i) 
        );
   end generate gen_latches;    

    -- First qubit
    qubit_0: qubit port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r, 
        enable => enable, 
        reset => reset,
        m_2 => '0',
        m_1 => m(0),
        m_0 => m(1),
        prog => prog(15 downto 0),
        ops_above => "00",
        ops => ops_internal(1 downto 0),
        ops_below => ops_internal(3 downto 2),
        s => s(0)
    );
 
    -- Generate middle qubits
    gen_qubits: for i in 1 to N-2 generate
        qubit_i : qubit port map (
            clk_p => clk_p, 
            clk_s => clk_s, 
            clk_r => clk_r, 
            enable => enable, 
            reset => reset,
            m_2 => m(i-1),
            m_1 => m(i),
            m_0 => m(i+1),
            prog => prog(16*i+15 downto 16*i),
            ops_above => ops_internal(2*i-1 downto 2*(i-1)),
            ops => ops_internal(2*(i+1)-1 downto 2*i),
            ops_below => ops_internal(2*(i+2)-1 downto 2*(i+1)),
            s => s(i)     
        );
   end generate gen_qubits;
   
    
     -- Last qubit
    qubit_last: qubit port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r, 
        enable => enable, 
        reset => reset,
        m_2 => m(N-2),
        m_1 => m(N-1),
        m_0 => '0',
        prog => prog(16*N-1 downto 16*(N-1)),
        ops_above => ops_internal(2*(N-1)-1 downto 2*(N-2)),
        ops => ops_internal(2*N-1 downto 2*(N-1)),
        ops_below => "00",
        s => s(N-1)
    );

end behav;
