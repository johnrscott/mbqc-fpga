--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file comm_correct.vhd
--! \brief The CNOT commutation correction module

library ieee;
use ieee.std_logic_1164.all;

--!
--! \brief Commutation correction module
--!
--! This module is used to perform the CNOT 
--! commutation correction, and to add constants
--! to the byproduct operators. 
--!
--! The module is synchronous. There is no need
--! for a reset input because there is no internal
--! state. If either c_prog(1) or c_prog(4) is 
--! high on the rising edge of the clock, then the 
--! output comm will be computed based on the 
--! byproduct operators above and below.
--!
--! \param clk_r The clock input, which will be the
--! system reset clock clk_r
--!
--! \param c_prog The commutation correction program.
--! The meaning of the bits is as follows. Bit 0 is 
--! not used in this module (it indicates that the
--! byproduct operators must be stored). Bit 1 high
--! means that a CNOT commutation correction is necessary.
--! In that case, bit 2 high means that the current
--! logical qubit is the control, and bit 3 high indicates
--! that the other logical qubit is above. Bit 4 high
--! indicates that a constant must be added to the byproduct
--! operators. In this case, bit 2 is added to z and bit
--! 3 is added to x.
--!
--! \param ops_above The byproduct operators for the qubit
--! above.
--!
--! \param ops_below The byproduct operators for the qubit
--! below.
--!
--! \param comm The commutation correction output. This 
--! is zero if the enable input is low, and is computed from
--! the byproduct operator inputs according to the program
--! inputs if the enable input is high. It should be added to
--! the current byproduct operators in a parent module.
--!
--!
--!
entity comm_correct is
port (
    clk_r: in std_logic;
    c_prog: in std_logic_vector(4 downto 0);
    ops_above, ops_below : in std_logic_vector (1 downto 0);
    comm: out std_logic_vector (1 downto 0)
);
end comm_correct;

architecture behavioural of comm_correct is
begin

    --! On the rising edge of clk_r, it might
    --! be necessary to make a commutation 
    --! correction if the next gate is a CNOT
    --! gate. The job of this process is to
    --! set a register comm which, when added
    --! to the byproduct operators (outside this
    --! module), performs the correction.
    --!
    --! The correction should only be performed if
    --! the enable input is high, which means that 
    --! the next gate is a CNOT gate. 
    proc_r: process (clk_r) is
    begin
        if clk_r'event and clk_r = '1'
        then
            if c_prog(4) = '1'
            then
                -- Add constants to the byproduct 
                -- operators-
                comm <= c_prog(3 downto 2);
            elsif c_prog(1) = '1'
            then
                -- In this case a CNOT byproduct
                -- commutation correction is
                -- necessary.
                if c_prog(3 downto 2) = "11"
                then
                    -- Qubit is control, target above
                    -- Add z_above to z
                    comm(0) <= ops_above(0);
                    comm(1) <= '0';
                
                elsif c_prog(3 downto 2) = "10"
                then
                    -- Qubit is target, control above
                    -- Add x_above to x
                    comm(0) <= '0';
                    comm(1) <= ops_above(1);        
                
                elsif c_prog(3 downto 2) = "01"
                then            
                    -- Qubit is control, target below
                    -- Add z_below to z
                    comm(0) <= ops_below(0);
                    comm(1) <= '0';
                
                elsif c_prog(3 downto 2) = "00"
                then
                    -- Qubit is target, control below
                    -- Add x_below to x
                    comm(0) <= '0';
                    comm(1) <= ops_below(1);                 
                end if;
            else
                -- If neither bit is set, then set
                -- comm to zero so as not to modify
                -- the byproduct operators
                comm <= "00";        
            end if;
        end if;
    end process proc_r;
end behavioural;
