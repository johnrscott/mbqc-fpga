--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file qubit.vhd
--! \brief Logical qubit module

library ieee;
use ieee.std_logic_1164.all;

--! \brief Logical qubit module
--!
--! m_2 is from the qubit above, m_1 is from
--! the current qubit, and m_0 is from the qubit
--! below.
--! 
entity qubit is
port ( 
    clk_p, clk_s, clk_r, enable, reset : in std_logic;
    m_0, m_1, m_2: in std_logic;
    prog: in std_logic_vector(15 downto 0);
    ops_above, ops_below: in std_logic_vector(1 downto 0);
    ops: out std_logic_vector(1 downto 0);
    s: out std_logic
);
end qubit;

--! 
--! \brief Implementation of qubit moduel
--!
architecture behav of qubit is

    component adapt is
    port ( 
        clk_p, clk_s, clk_r, input, enable, reset : in std_logic;
        p: in std_logic_vector(4 downto 0);
        ops_stored: in std_logic_vector(1 downto 0);
        s: out std_logic
    );
    end component adapt;

    component byproduct is
    port ( 
        clk_p, clk_s, clk_r, reset, enable : in std_logic;
        b_prog : in std_logic_vector (5 downto 0);
        c_prog: in std_logic_vector (4 downto 0);
        m : in std_logic_vector (2 downto 0);
        ops_above, ops_below : in std_logic_vector (1 downto 0);
        ops_current, ops_stored : out std_logic_vector (1 downto 0)
    );
    end component byproduct;
    
    --! Stored byproduct operators
    signal ops_stored: std_logic_vector(1 downto 0);
    
    --! Program signals
    signal c_prog: std_logic_vector(4 downto 0);
    signal s_prog: std_logic_vector(4 downto 0);
    signal b_prog: std_logic_vector(5 downto 0);

    --! Measurement signal
    signal meas: std_logic_vector(2 downto 0);

begin
    
    -- Set the program inputs
    c_prog <= prog(15 downto 11);
    s_prog <= prog(10 downto 6);
    b_prog <= prog(5 downto 0);
    
    -- Set the measurement inputs
    meas <= m_2 & m_1 & m_0;
    
    a0: adapt port map(
        reset => reset,
        enable => enable,
        input => meas(1), -- This qubit's measurement outcome
        clk_p => clk_p,
        clk_s => clk_s,
        clk_r => clk_r,
        p => s_prog,
        ops_stored => ops_stored,
        s => s
    );
    
    b0: byproduct port map(
        reset => reset,
        enable => enable,
        m => meas,
        clk_p => clk_p,
        clk_s => clk_s,
        clk_r => clk_r,
        c_prog => c_prog,
        b_prog => b_prog,
        ops_current => ops,
        ops_stored => ops_stored,
        ops_above => ops_above,
        ops_below => ops_below
    );

end behav;