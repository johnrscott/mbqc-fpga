--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file mbqc2_tb.vhd
--! \brief Testbench for the multiple qubit MBQC system

library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;

library work;
use work.example_package.to_hstring;

entity mbqc2_tb is
generic (
    N: integer := 4
);
end mbqc2_tb;

--!
--! \brief Test the mbqc2 design from file
--!
architecture file_test of mbqc2_tb is

    component mbqc2_wrapper is
    port (
        clk_p_in : in std_logic;
        enable : in std_logic;
        locked : out std_logic;
        meas : in std_logic_vector(N-1 downto 0);
        ops : out std_logic_vector(2*N-1 downto 0);
        reset : in std_logic;
        s : out std_logic_vector(N-1 downto 0)
    );
    end component mbqc2_wrapper;
    
    component clocks is
    generic (
        T: time;
        start_delay: time
    );
    port (
        clk_p, clk_s, clk_r: out std_logic
    );
    end component;

    signal clk_p, clk_p_in, clk_r, clk_s, reset, enable : STD_LOGIC;
    signal meas: std_logic_vector(N-1 downto 0);
    signal ops: std_logic_vector(2*N-1 downto 0);
    signal s: std_logic_vector(N-1 downto 0);
    signal locked: std_logic;
    
    signal ops_true: std_logic_vector(2*N-1 downto 0);   
    signal s_true: std_logic_vector(N-1 downto 0);
    
    signal linenum: integer;

    constant T: time := 10ns; 
    constant D: time := 2ns; 

    file test_file : text;
    shared variable iline: line;
    
    signal finished: boolean := false;
    
    --! This line number is used to indicate the
    --! current position in the test file.

    
    --! \brief Procedure for reading the test file
    --!
    --! This function reads the next line from the
    --! testbench test file and sets all the signal
    --! to their correct values. It should be called
    --! on the rising edge of clk_p.
    --!
    --!
    procedure read_file(
        variable test_file: in text; 
        signal linenum: inout integer;
        signal meas: out std_logic_vector(N-1 downto 0);
        signal ops: out std_logic_vector(2*N-1 downto 0);
        signal s: out std_logic_vector(N-1 downto 0)
    ) is
        variable linenum_var: integer;
        variable meas_var: std_logic_vector(N-1 downto 0);
        variable ops_var: std_logic_vector(2*N-1 downto 0);
        variable s_var: std_logic_vector(N-1 downto 0);
    
        variable iline: line;
        variable sep: character; -- the file separator
    begin
    
        -- Read a new line
        readline(test_file, iline);
    
        -- Read all the variables from the line
        read(iline, linenum_var);
        read(iline, sep); -- read the separator
        read(iline, meas_var);
        read(iline, sep);    
        read(iline, ops_var);
        read(iline, sep);
        read(iline, s_var);      
        -- There is no final separator to read
        
        -- Copy the variables to the output signals
        meas <= meas_var;
        ops <= ops_var;
        s <= s_var;
        linenum <= linenum_var;
        
    end procedure read_file;

    --

begin

    -- Instantiate clocks
    clk: clocks
    generic map (
        T => T,
        start_delay => D
    )
    port map (
        clk_p => clk_p_in,
        clk_r => clk_r -- Used in the testbench
    );

    uut : mbqc2_wrapper port map (
        clk_p_in => clk_p_in,
        reset => reset,
        enable => enable,
        locked => locked,
        meas => meas,
        ops => ops,
        s => s
    );
    
    --! This process is used to set the initial delay
    --! before the clocks start up.
    setup: process is
    begin
            
        -- Open file and read the header row to discard 
        file_open(test_file, "simdir/mtb.csv",  read_mode);
        readline(test_file, iline); 
    
        -- Set initial values of the inputs
        reset <= '1';
        enable <= '0';
    
        wait until locked = '1';
    
        wait for 50ns; 
        reset <= '0';
        wait for 50ns;
        
        -- Perform a second reset now that the 
        -- clocks are running
        reset <= '1';
        wait for 50ns;
        reset <= '0';
        
        wait for 10ns;
        
        --! Wait for the rising edge of clk_r
        --! to enable the module. This can be
        --! left until clk_p, but in order for
        --! the file read to happen on the first
        --! clk_p, enable should be set beforehand
        wait until rising_edge(clk_r);
        enable <= '1';
        
        --! Wait for the process to finish
        wait until finished = true;
        report "finished";
        enable <= '0';
        wait for 150ns;
        std.env.finish;
        
        
    end process setup;

    proc_p: process(clk_p_in) is
    begin
        if rising_edge(clk_p_in) and enable = '1'
        then
            if not endfile(test_file)
            then
                -- Read the next line from the file
                -- and assign values to signals
                read_file(
                    test_file,
                    linenum => linenum,
                    meas => meas,
                    ops => ops_true,
                    s => s_true
                );
            else
                file_close(test_file);
                finished <= true;
                report "finished";   
            end if;
        elsif falling_edge(clk_p_in)
        then
            --! Reset the measurement input back to zero
            meas <= (others => '0');
        end if;
    end process proc_p;
    
    --! The rising edge of clk_s is when the byproduct operator
    --! output changes, so it must be checked on the rising edge
    --! of clk_r to avoid a race condition.
    --!
    --! The adaptive setting s is also checked here
    --!
    proc_r: process(clk_r) is
    begin
        if rising_edge(clk_r) and enable = '1'
        then
            assert(ops=ops_true) report "At line " & integer'image(linenum) 
                & ", ops = " & to_hstring(ops) & ", should be " &  to_hstring(ops_true);
            assert(s=s_true) report "At line " & integer'image(linenum) 
                & ", s = " & to_hstring(s) & ", should be " &  to_hstring(s_true);
--            assert(prog=prog_true) report "At line " & integer'image(linenum) 
--                & ", prog = " & to_hstring(prog) & ", should be " &  to_hstring(prog_true);
        end if;
    end process proc_r;

end file_test;

