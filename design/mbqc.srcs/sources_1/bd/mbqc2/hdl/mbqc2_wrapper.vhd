--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
--Date        : Mon Oct 11 10:40:26 2021
--Host        : hp-jrs running 64-bit Linux Mint 20.1
--Command     : generate_target mbqc2_wrapper.bd
--Design      : mbqc2_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mbqc2_wrapper is
  port (
    clk_p_in : in STD_LOGIC;
    enable : in STD_LOGIC;
    locked : out STD_LOGIC;
    meas : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ops : out STD_LOGIC_VECTOR ( 7 downto 0 );
    reset : in STD_LOGIC;
    s : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end mbqc2_wrapper;

architecture STRUCTURE of mbqc2_wrapper is
  component mbqc2 is
  port (
    clk_p_in : in STD_LOGIC;
    reset : in STD_LOGIC;
    enable : in STD_LOGIC;
    locked : out STD_LOGIC;
    ops : out STD_LOGIC_VECTOR ( 7 downto 0 );
    s : out STD_LOGIC_VECTOR ( 3 downto 0 );
    meas : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  end component mbqc2;
begin
mbqc2_i: component mbqc2
     port map (
      clk_p_in => clk_p_in,
      enable => enable,
      locked => locked,
      meas(3 downto 0) => meas(3 downto 0),
      ops(7 downto 0) => ops(7 downto 0),
      reset => reset,
      s(3 downto 0) => s(3 downto 0)
    );
end STRUCTURE;
