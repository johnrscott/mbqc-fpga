--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
--Date        : Tue Oct  5 15:27:09 2021
--Host        : hp-jrs running 64-bit Linux Mint 20.1
--Command     : generate_target mbqc1_wrapper.bd
--Design      : mbqc1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mbqc1_wrapper is
  port (
    clk_p_in : in STD_LOGIC;
    enable : in STD_LOGIC;
    locked : out STD_LOGIC;
    meas : in STD_LOGIC;
    ops : out STD_LOGIC_VECTOR ( 1 downto 0 );
    reset : in STD_LOGIC;
    s : out STD_LOGIC
  );
end mbqc1_wrapper;

architecture STRUCTURE of mbqc1_wrapper is
  component mbqc1 is
  port (
    enable : in STD_LOGIC;
    locked : out STD_LOGIC;
    s : out STD_LOGIC;
    ops : out STD_LOGIC_VECTOR ( 1 downto 0 );
    meas : in STD_LOGIC;
    reset : in STD_LOGIC;
    clk_p_in : in STD_LOGIC
  );
  end component mbqc1;
begin
mbqc1_i: component mbqc1
     port map (
      clk_p_in => clk_p_in,
      enable => enable,
      locked => locked,
      meas => meas,
      ops(1 downto 0) => ops(1 downto 0),
      reset => reset,
      s => s
    );
end STRUCTURE;
