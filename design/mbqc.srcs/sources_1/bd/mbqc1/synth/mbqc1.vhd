--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
--Date        : Tue Oct  5 15:27:08 2021
--Host        : hp-jrs running 64-bit Linux Mint 20.1
--Command     : generate_target mbqc1.bd
--Design      : mbqc1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mbqc1 is
  port (
    clk_p_in : in STD_LOGIC;
    enable : in STD_LOGIC;
    locked : out STD_LOGIC;
    meas : in STD_LOGIC;
    ops : out STD_LOGIC_VECTOR ( 1 downto 0 );
    reset : in STD_LOGIC;
    s : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of mbqc1 : entity is "mbqc1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=mbqc1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=4,numReposBlks=4,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=1,numPkgbdBlks=0,bdsource=USER,synth_mode=Global}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of mbqc1 : entity is "mbqc1.hwdef";
end mbqc1;

architecture STRUCTURE of mbqc1 is
  component mbqc1_c_counter_binary_0_0 is
  port (
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  end component mbqc1_c_counter_binary_0_0;
  component mbqc1_clk_wiz_0_0 is
  port (
    reset : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    clkfb_in : in STD_LOGIC;
    clk_p : out STD_LOGIC;
    clk_s : out STD_LOGIC;
    clk_r : out STD_LOGIC;
    clkfb_out : out STD_LOGIC;
    locked : out STD_LOGIC
  );
  end component mbqc1_clk_wiz_0_0;
  component mbqc1_dist_mem_gen_0_0 is
  port (
    a : in STD_LOGIC_VECTOR ( 7 downto 0 );
    clk : in STD_LOGIC;
    qspo : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component mbqc1_dist_mem_gen_0_0;
  component mbqc1_single_qubit_0_0 is
  port (
    clk_p : in STD_LOGIC;
    clk_s : in STD_LOGIC;
    clk_r : in STD_LOGIC;
    enable : in STD_LOGIC;
    reset : in STD_LOGIC;
    meas : in STD_LOGIC;
    prog : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ops : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s : out STD_LOGIC
  );
  end component mbqc1_single_qubit_0_0;
  signal c_counter_binary_0_Q : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal clk_p_in_1 : STD_LOGIC;
  signal clk_wiz_0_clk_p : STD_LOGIC;
  signal clk_wiz_0_clk_r : STD_LOGIC;
  signal clk_wiz_0_clk_s : STD_LOGIC;
  signal clk_wiz_0_locked : STD_LOGIC;
  signal dist_mem_gen_0_qspo : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal enable_1 : STD_LOGIC;
  signal meas_1 : STD_LOGIC;
  signal reset_1 : STD_LOGIC;
  signal single_qubit_0_ops : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal single_qubit_0_s : STD_LOGIC;
  signal NLW_clk_wiz_0_clkfb_out_UNCONNECTED : STD_LOGIC;
begin
  clk_p_in_1 <= clk_p_in;
  enable_1 <= enable;
  locked <= clk_wiz_0_locked;
  meas_1 <= meas;
  ops(1 downto 0) <= single_qubit_0_ops(1 downto 0);
  reset_1 <= reset;
  s <= single_qubit_0_s;
c_counter_binary_0: component mbqc1_c_counter_binary_0_0
     port map (
      CE => enable_1,
      CLK => clk_wiz_0_clk_s,
      Q(7 downto 0) => c_counter_binary_0_Q(7 downto 0),
      SCLR => '0'
    );
clk_wiz_0: component mbqc1_clk_wiz_0_0
     port map (
      clk_in1 => clk_p_in_1,
      clk_p => clk_wiz_0_clk_p,
      clk_r => clk_wiz_0_clk_r,
      clk_s => clk_wiz_0_clk_s,
      clkfb_in => clk_wiz_0_clk_p,
      clkfb_out => NLW_clk_wiz_0_clkfb_out_UNCONNECTED,
      locked => clk_wiz_0_locked,
      reset => '0'
    );
dist_mem_gen_0: component mbqc1_dist_mem_gen_0_0
     port map (
      a(7 downto 0) => c_counter_binary_0_Q(7 downto 0),
      clk => clk_wiz_0_clk_p,
      qspo(15 downto 0) => dist_mem_gen_0_qspo(15 downto 0)
    );
single_qubit_0: component mbqc1_single_qubit_0_0
     port map (
      clk_p => clk_wiz_0_clk_p,
      clk_r => clk_wiz_0_clk_r,
      clk_s => clk_wiz_0_clk_s,
      enable => enable_1,
      meas => meas_1,
      ops(1 downto 0) => single_qubit_0_ops(1 downto 0),
      prog(15 downto 0) => dist_mem_gen_0_qspo(15 downto 0),
      reset => reset_1,
      s => single_qubit_0_s
    );
end STRUCTURE;
