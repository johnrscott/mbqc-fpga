--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file byproduct_tb.vhd
--! \brief Testbench for the byproduct operator subsystem

library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity byproduct_tb is
end byproduct_tb;

--!
--! \brief Full test from file
--!
--! This testbench is for testing that the
--! system responds correctly to simulated
--! input that is read from a file. The
--! file also contains the expected output,
--! which is used to check that the system
--! works correctly.
--!
architecture file_test of byproduct_tb is
    
    --! \brief Procedure for reading the test file
    --!
    --! This function reads the next line from the
    --! testbench test file and sets all the signal
    --! to their correct values. It should be called
    --! on the rising edge of clk_p.
    --!
    --!
    procedure read_file(
        variable test_file: in text;
        signal line_num: inout integer;
        signal reset: out std_logic;
        signal enable: out std_logic;
        signal b_prog: out std_logic_vector(5 downto 0);
        signal c_prog: out std_logic_vector(4 downto 0);
        signal meas: out std_logic_vector(2 downto 0);
        signal ops_above: out std_logic_vector(1 downto 0);
        signal ops_below: out std_logic_vector(1 downto 0);
        signal ops_stored: out std_logic_vector(1 downto 0);
        signal ops_current: out std_logic_vector(1 downto 0)
    ) is
        variable reset_var: std_logic;
        variable enable_var: std_logic;
        variable c_prog_var: std_logic_vector(4 downto 0);
        variable s_prog_var: std_logic_vector(4 downto 0);
        variable b_prog_var: std_logic_vector(5 downto 0);
        variable meas_var: std_logic_vector(2 downto 0);
        variable ops_above_var: std_logic_vector(1 downto 0);
        variable ops_below_var: std_logic_vector(1 downto 0);
        variable ops_stored_var: std_logic_vector(1 downto 0);
        variable ops_current_var: std_logic_vector(1 downto 0);
        variable s_var: std_logic; -- unused in this testbench
    
        variable iline: line;
        variable sep: character; -- the file separator
    begin
    
        -- Increment the line number
        line_num <= line_num + 1;
    
        -- Read a new line
        readline(test_file, iline);
    
        -- Read all the variables from the line
        read(iline, reset_var);
        read(iline, sep); -- read the separator
        read(iline, enable_var);
        read(iline, sep);
        read(iline, c_prog_var);
        read(iline, sep);
        read(iline, s_prog_var);
        read(iline, sep);
        read(iline, b_prog_var);
        read(iline, sep);
        read(iline, meas_var);
        read(iline, sep);    
        read(iline, ops_above_var);
        read(iline, sep);
        read(iline, ops_below_var);
        read(iline, sep);
        read(iline, ops_stored_var);
        read(iline, sep);
        read(iline, ops_current_var);
        read(iline, sep);
        read(iline, s_var);        
        -- There is no final separator to read
        
        -- Copy the variables to the output signals
        reset <= reset_var;
        enable <= enable_var;
        c_prog <= c_prog_var;
        b_prog <= b_prog_var;
        meas <= meas_var;
        ops_above <= ops_above_var;
        ops_below <= ops_below_var;
        ops_stored <= ops_stored_var;
        ops_current <= ops_current_var;
        
    end procedure read_file;
    
    component byproduct is
    port ( 
        clk_p, clk_s, clk_r, reset, enable : in std_logic;
        c_prog : in std_logic_vector (4 downto 0);
        b_prog : in std_logic_vector (5 downto 0);
        m : in std_logic_vector (2 downto 0);
        ops_above, ops_below : in std_logic_vector (1 downto 0);
        ops_current, ops_stored : out std_logic_vector (1 downto 0)
    );
    end component byproduct;
    
    component clocks is
    generic (
        T: time;
        start_delay: time
    );
    port (
        clk_p, clk_s, clk_r: out std_logic
    );
    end component;

    signal clk_p, clk_s, clk_r, reset, enable : STD_LOGIC;
    signal c_prog : STD_LOGIC_VECTOR (4 downto 0);
    signal b_prog : STD_LOGIC_VECTOR (5 downto 0);
    signal m : STD_LOGIC_VECTOR (2 downto 0);
    signal ops_above, ops_below : STD_LOGIC_VECTOR (1 downto 0);
    signal ops_current, ops_stored : STD_LOGIC_VECTOR (1 downto 0);
    
    -- Correct outputs, read from file, to compare the
    -- ops_current and ops_stored signals to
    signal ops_current_true, ops_stored_true : STD_LOGIC_VECTOR (1 downto 0);
    
    constant T: time := 20ns; 
    constant D: time := 2ns; 
        
    --! The test_file is space separated and
    --! has the following fields:
    --!
    --! 1) reset and enable, each one bit, for the
    --!    enable and reset signals.
    --! 2) prog, 10 bits, for the program input.
    --! 3) meas, 3 bits, for the measurement latch
    --!    results from the current and nearest
    --!    neighbour qubits.
    --! 4) ops_above, 2 bits, for the byproduct 
    --! 5) operators for the qubit above.
    --! 6) ops_below, 2 bits, for the byproduct
    --!    operators for the qubit below.
    --!
    --!
    file test_file : text;
    shared variable iline: line;
    
    --! This line number is used to indicate the
    --! current position in the test file.
    signal line_num: integer := 0; 
            
begin

    -- Instantiate clocks
    clk: clocks
    generic map (
        T => T,
        start_delay => D
    )
    port map (
        clk_p => clk_p,
        clk_s => clk_s,
        clk_r => clk_r
    );

    uut : byproduct port map (
        clk_p => clk_p, 
        clk_s => clk_s, 
        clk_r => clk_r,
        reset => reset,
        enable => enable,
        c_prog => c_prog,
        b_prog => b_prog,
        m => m,
        ops_above => ops_above,
        ops_below => ops_below,
        ops_current => ops_current,
        ops_stored => ops_stored
    );
    


    --! This process is used to set the initial delay
    --! before the clocks start up.
    setup: process is
    begin
        file_open(test_file, "byp-test.csv",  read_mode);
        readline(test_file, iline); -- Read the header row to discard 
        wait; -- indefinitely
    end process setup;
    
    --! Update the program on rising edge of clk_p. The measurement
    --! latches m should also be updated on the rising edge clk_p, which 
    --! simulates the latch output when photon arrives at the detector.
    --!
    --! All the signals are read from the file in this process. For the
    --! signals prog and meas, this is accurate because those signals
    --! change on the rising edge of clk_p in the real system, For 
    --! ops_* signals, the signal will change before clk_p (on either
    --! the previous clk_s or clk_r). Since they are read on the next
    --! clk_s, this should not be a problem in this testbench.
    --!
    --! \todo To make the testbench more realistic, assert the ops_*
    --! signals on clk_s and clk_r instead of here.
    --!
    --! 
    proc_p: process(clk_p) is
    begin
        if rising_edge(clk_p)
        then
            if not endfile(test_file)
            then
                -- Read the next line from the file
                -- and assign values to signals
                read_file(
                    test_file,
                    line_num => line_num,
                    reset => reset,
                    enable => enable,
                    c_prog => c_prog,
                    b_prog => b_prog,
                    meas => m,
                    ops_above => ops_above,
                    ops_below => ops_below,
                    ops_stored => ops_stored_true,
                    ops_current => ops_current_true
                );
            else
                file_close(test_file);
                std.env.finish;                
            end if;
        end if;
    end process proc_p;
    
    
    --! On the rising edge of clk_s, the byproduct operator
    --! systems for the neighbouring qubits update their
    --! operaters, which means the ops_above and ops_below
    --! inputs should change here. They are read from the 
    --! same line of the input file that was loaded on the
    --! rising edge of clk_p.
    --!
    --! The stored byproduct operators can also be checked
    --! here, because this is when they get used by the
    --! adaptive measurement system.
    --!
    --!
    proc_s: process(clk_s) is     
    begin
        if rising_edge(clk_s)
        then  
            assert(ops_stored=ops_stored_true) report "At line " & integer'image(line_num) 
                & ", ops_stored = " & to_hstring(ops_stored) & ", should be " &  to_hstring(ops_stored_true);
        end if;
    end process proc_s;
    
    --! The rising edge of clk_s is when the byproduct operator
    --! output changes, so it must be checked on the rising edge
    --! of clk_r to avoid a race condition.
    --!
    proc_r: process(clk_r) is
    begin
        if rising_edge(clk_r)
        then
            assert(ops_current=ops_current_true) report "At line " & integer'image(line_num) 
                & ", ops_current = " & to_hstring(ops_current) & ", should be " &  to_hstring(ops_current_true);
        end if;
    end process proc_r;
    
end file_test;