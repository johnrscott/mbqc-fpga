--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file clocks.vhd
--! \brief Clock generator for use in simulations
--!
--! This module generates the three clocks clk_p
--! clk_s and clk_r that are required by the 
--! designs.
--!
--! This module is not for use in designs. Use the
--! clocking wizard instead. 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


--! 
--! \brief Clock generator module
--!
--! This module generates three out of
--! phase clocks: clk_p is the photon
--! clock, clk_s is the sample clock,
--! and clk_r is the reset clock. The
--! module is parametrised based on the
--! clock period.
--!
--! The three clocks are generated 
--! 120 degrees out of phase so that
--! they are equally spaced.
--!
--! \param enable 
--!
--!
entity clocks is
    generic (
        T: time;
        start_delay: time
        --!\todo Add phase shifts here
    );
    port (
        clk_p, clk_s, clk_r: out STD_LOGIC := '0'
    );
end clocks;

architecture behaviour of clocks is

begin    

    photon_clock: process is
        variable first: boolean := true;
    begin
        if first = true
        then
            first := false;
            wait for start_delay;
        end if; 
        clk_p <= '0';
        wait for T/2;
        clk_p <= '1';
        wait for T/2;
    end process photon_clock;
    
    sample_clock: process is
        variable first: boolean := true;
    begin
        -- Set up initial phase delay
        if first = true
        then
            first := false;
            wait for start_delay + T/3;
        end if;
    
        -- toggle clock
        clk_s <= '0';
        wait for T/2;
        clk_s <= '1';
        wait for T/2;
    end process sample_clock;
    
    reset_clock: process is
        variable first: boolean := true;
    begin
        -- Set up initial phase delay
        if first = true
        then
            first := false;
            wait for start_delay + 2*T/3;
        end if;
    
        -- toggle clock
        clk_r <= '0';
        wait for T/2;
        clk_r <= '1';
        wait for T/2;
    end process reset_clock;      

end behaviour;
