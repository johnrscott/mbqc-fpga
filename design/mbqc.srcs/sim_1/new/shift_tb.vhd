--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file shift_tb.vhd
--! \brief Shift register testbench

library ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity shift_tb is			-- entity declaration
end shift_tb;

architecture tb of shift_tb is

    component shift_reg
    port(
        input, clock, enable, reset: in std_logic;
	    output: out std_logic_vector(2 downto 0)
    );
    end component;

    signal tb_input, tb_clock, tb_enable, tb_reset: std_logic;
    signal tb_output: std_logic_vector(2 downto 0);

begin

    UUT: shift_reg port map (
        input => tb_input,
        clock => tb_clock,
        enable => tb_enable,
        reset => tb_reset,
        output => tb_output
    );
	
    -- concurrent process of clock 10ns period
    -- The clock has a rising edge on 5ns,
    -- 15ns, 25ns, etc, which means data should
    -- be changed on multiples of 10ns. The output
    -- changes on rising edges of the clock, and 
    -- should be read at the next higher multiple
    -- of 10ns
    clock: process is
    begin
	   tb_clock <= '0';
	   wait for 5 ns;
	   tb_clock <= '1';
	   wait for 5 ns;
    end process clock;

    -- Send serial data to the shift
    test_in: process is
	variable err_cnt: integer := 0;
    begin
    
        -- Initialise reset
        tb_reset <= '0';
        
        -- Enable shifting 					
        tb_enable <= '1'; 
	   
        -- Send the following string of
        -- serial data to the input:
        -- 001101. This should produce
        -- the following outputs at each
        -- rising edge of the clock:
        --
        -- clk, in, enable, reset, out
        -- 0,   0,  1,      0,     000
        -- 1,   0,  1,      0,     000 
        -- 2,   1,  1,      0,     100
        -- 3,   1,  1,      0,     110  
        -- 4,   0,  1,      0,     011
        -- 5,   1,  0,      0,     011
        -- 6,   1,  0,      0,     011
        -- 7,   1,  0,      1,     000
        --
        tb_input <= '0'; -- clock edge 0
        wait for 20 ns; -- clock edge 1
        tb_input <= '1'; -- clock edge 2
        wait for 20 ns; -- clock edge 3
        tb_input <= '0'; -- clock edge 4
        wait for 10 ns;
        tb_input <= '1'; -- clock edge 5
	   
        -- Test enable input. This
        -- should freeze the current 
        -- value of the output, 011,
        -- for 2 clock cycles
        -- 
        tb_enable <= '0';
        wait for 20 ns;
        
        -- Test reset input. This
        -- should reset the output
        -- to 000. The reset is
        -- valid even when enable is
        -- low
        tb_reset <= '1';
	   
        wait;
    end process test_in;
	
	-- The results of the test above
	-- should be the following:
	--
	-- clk, in, enable, reset, out
    -- 0,   0,  1,      0,     000
    -- 1,   0,  1,      0,     000 
    -- 2,   1,  1,      0,     100
    -- 3,   1,  1,      0,     110  
    -- 4,   0,  1,      0,     011
    -- 5,   1,  0,      0,     011
    -- 6,   1,  0,      0,     011
    -- 7,   1,  0,      1,     000
	--
    test_out: process is
	variable err_cnt: integer := 0; 
    begin

        -- clk = 0
        wait for 10ns;
        assert(tb_output="000") report "Test clk=0 failed, out = " & to_hstring(tb_output)
        severity error;			 
        if (tb_output/="000") 
        then
            err_cnt:=err_cnt+1;
        end if;
        
        -- clk = 1
        wait for 10 ns;
        assert(tb_output="000") report "Test clk=1 failed, out = " & to_hstring(tb_output)
        severity error;			 
        if (tb_output/="000") 
        then
            err_cnt:=err_cnt+1;
        end if;
        
        -- clk = 2
        wait for 10 ns;
        assert(tb_output="100") report "Test clk=2 failed, out = " & to_hstring(tb_output)
        severity error;             
        if (tb_output/="100") 
        then
            err_cnt:=err_cnt+1;
        end if;
        
        -- clk = 3
        wait for 10 ns;
        assert(tb_output="110") report "Test clk=3 failed, out = 0x" & to_hstring(tb_output)
        severity error;             
        if (tb_output/="110") 
        then
            err_cnt:=err_cnt+1;
        end if;
        
        -- clk = 4
        wait for 10 ns;
        assert(tb_output="011") report "Test clk=4 failed, out = 0x" & to_hstring(tb_output)
        severity error;             
        if (tb_output/="011") 
        then
            err_cnt:=err_cnt+1;
        end if;

        -- clk = 5, enable test
        wait for 10 ns;
        assert(tb_output="011") report "Enable test clk=5 failed, out = 0x" & to_hstring(tb_output)
        severity error;             
        if (tb_output/="011") 
        then
            err_cnt:=err_cnt+1;
        end if;
        
        -- clk = 7, reset test
        wait for 20 ns;
        assert(tb_output="000") report "Reset test clk=7 failed, out = 0x" & to_hstring(tb_output)
        severity error;             
        if (tb_output/="000") 
        then
            err_cnt:=err_cnt+1;
        end if;        

        -- summary of all the tests
        if (err_cnt=0) then 
            assert (false) 
            report "Testbench of Shifter completed successfully"
            severity note;
        else
            assert (true)
            report "Something wrong, try again!"
            severity error;
        end if;
			
	wait;

    end process;

end tb;

----------------------------------------------------------------
configuration cfg_tb of shift_tb is
	for tb
	   for UUT: shift_reg
	       use entity work.shift_reg;
	   end for;
	end for;
end CFG_TB;
-----------------------------------------------------------------