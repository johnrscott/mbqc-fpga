--
--  Copyright 2021 John Scott
--
--  This file is part of mbqcfpga.
--
--  mbqcfpga is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  mbqcfpga is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with mbqcfpga.  If not, see <https://www.gnu.org/licenses/>.
--

--! \file latch_tb.vhd
--! \brief Latch module testbench
--!
--! This file contains the testbench for the latch module.
--! The input to the latch is connected to the output from
--! the photon detector, and the reset is connected to the
--! reset clock clk_r.
--!


library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;

--!
--! \brief Testbench for the latch module
--!
--! This testbench is designed to test that
--! the latch module behaves correctly. The
--! input to the latch is a short pulse 
--! which will come from the photon detector
--! The reset input is the clk_r signal from
--! the clock generator, which will reset
--! the latch to zero. The desired behaviour 
--! is that the the value on the output,
--! sampled at clk_s, should indicate whether
--! a photon pulse was present or not.
--!
entity latch_tb is
end latch_tb;

architecture tb of latch_tb is

    component latch is
    port (
        set : in STD_LOGIC;
        reset : in STD_LOGIC;
        q : out STD_LOGIC
    );
    end component;
    
    component clocks is
    generic (T: time);
    port (
        clk_p, clk_s, clk_r: out STD_LOGIC
    );
    end component;

    
    signal pulse : std_logic; -- photon click
    signal value : std_logic; -- output
    
    signal clk_p, clk_s, clk_r: std_logic;
    
    -- Clock period
    constant T: time := 20 ns;
    
    -- Photon pulse duration
    constant D: time := 1ns;
    
    -- Maximum number of clock cycles
    constant max_clk: natural := 13;
    
    -- Test vector for input
    type test_vector_array is array (natural range <>) of std_logic;
    constant test_vector: test_vector_array := (
        '1','1','0','1','0','0','1', '1', '0', '1', '1', '1', '0', '1'
    ); 
    
    file output_buf : text; -- text is keyword
    
begin
    
    -- Unit under test
    UUT: latch port map (
        set => pulse,
        reset => clk_r,
        q => value
    );
    
    -- Instantiate clocks
    clk: clocks
    generic map (
        T => T
    )
    port map (
        clk_p => clk_p,
        clk_s => clk_s,
        clk_r => clk_r
    );

    stimulus: process is
    begin
         for k in 0 to max_clk 
         loop
            -- Wait for photon clock
            wait until rising_edge(clk_p);
            pulse <= test_vector(k);
            wait for D;
            pulse <= '0';
         end loop;

        wait;
    end process stimulus;
    
    -- save data in file : path is relative
    file_open(output_buf, "latch_data.csv", write_mode);
    process
        variable line_buf : line; -- line is keyword
    begin
        for i in 0 to max_clk
        loop
            wait until rising_edge(clk_s);

            if i = 0
            then
                -- Write file header
                write(line_buf, string'("clock_tick,pulse,sample,"));
                writeline(output_buf, line_buf);
            end if;
            
            write(line_buf, i);
            write(line_buf, string'(","));
            write(line_buf, test_vector(i));
            write(line_buf, string'(","));
            write(line_buf, value);
            write(line_buf, string'(","));
            
            -- Note that unsigned/signed values can not be saved in file, 
            -- therefore change into integer or std_logic_vector etc.
             -- following line saves the count in integer format
            --write(write_col_to_output_buf, to_integer(unsigned(count))); 
            writeline(output_buf, line_buf);
        end loop;
        wait;
    end process;
    
    measure: process is
    begin
        
    
        wait;
    end process measure;
    
end tb;

-- Bind components to specific entities
configuration cfg_tb of latch_tb is
    for tb
        for UUT: latch
            use entity work.latch(behaviour);
        end for;
        for clk: clocks
            use entity work.clocks(behaviour);
        end for;
    end for;
end configuration;