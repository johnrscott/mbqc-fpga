Simulating MBQC
###############

The following sections contain a description of the program **mbqcsim**, which is a command line utility for simulating MBQC circuits containing one-qubit gates and CNOT gates for the purpose of verifying the digital design.

Full documentation for all the code in the simulator is available `here <simdocs/index.html>`_. The following sections below contain an overview of the main parts of the program, for the purpose of getting a high level overview.

MBQC simulator
**************

The main class in the program is **MBQC**, which is a measurement-based quantum computing simulator:

.. doxygenclass:: MBQC

The member functions of this class are documented below.
   
.. doxygenclass:: MBQC
   :members:
   :members-only:

Cluster state simulator
***********************

The heart of the MBQC simulator is a cluster state simulator that can simulate an arbitrary depth cluster state by holding two columns of the cluster state in memory at any one time. The simulator is based on the resizing simulator in `QSL <https://github.com/lanamineh/qsl>`_. This means that the maximum number of cluster state rows N that can be simulated is half the maximum number of qubits that can be simulated using your computer, which for a laptop is roughly 28. The resizing simulator never reallocates the state vector, which makes the program execute quite fast. The class that performs this simulation is **ClusterState**:

.. doxygenclass:: ClusterState

It has the following member functions

.. doxygenclass:: ClusterState
   :members:
   :members-only:

Storing the program
*******************

The program is stored in the **Program** class, which stores a single program word, prescribing what measurement basis angle to use, and how to compute the next adaptive measurement settings and byproduct operators from the measurement outcome. 

.. doxygenclass:: Program

The member functions of this class are documented below.
   
.. doxygenclass:: Program
   :members:
   :members-only:


Logical qubit information
*************************

The class which stores logical qubit information (corresponding to one row in the cluster state) is the **LogicalQubit** class, documented below. The most important function is the ``storeOutcome()`` member function, which acts as the interface to the cluster state simulator. After the measurement outcomes from them cluster state simulator are obtained, (by calling the ``measureColumn()`` member function), the results are used in the arguments to ``storeOutcome()``, which computes the next adaptive measurement setting and byproduct operators for that measurement round.

.. doxygenclass:: LogicalQubit

The member functions of this class are documented below.
   
.. doxygenclass:: LogicalQubit
   :members:
   :members-only:


		  
VHDL testbenches
################

.. todo::
   Documentation coming soon
