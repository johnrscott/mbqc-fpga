License
#######

The code and documentation is covered by the GPLv3 License below.

.. literalinclude:: ../COPYING
