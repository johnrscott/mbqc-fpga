Finding documentation
#####################

Documentation for the hardware design can be found `here <vhdldocs/index.html>`_. On the main doxygen page, click on the `Design Unit List` tab, which contains documentation for all the entities in the VHDL design.

RTL design
##########

The hardware design is written in VHDL. The functioning of the three main entities in the program is described below.

Adaptive measurement settings
*****************************

The ``adapt`` entity contains the calculation of the adaptive measurement setting from the measurement outcomes and stored byproduct operators (which are both inputs). It contains a shift register for keeping track of the last three measurement outcomes, and registers to contain the program mask and the byproduct operator term (both of which must be valid through the rising edge of ``clk_p``).

The schematic of the ``adapt`` module is shown below (click for full size image):

.. image:: pictures/adapt.png
  :alt: Adaptive measurement setting block
  :target: _images/adapt.png

After the output of the shift register, the value of ``s`` is calculated using combinational logic so as to avoid the need for another clock edge after ``clk_s``.
	
Byproduct operators
*******************

The byproduct operators are calculated using the ``byproduct`` entity. On the rising edge of ``clk_s`` the byproduct operators are calculated by adding up three terms: the current value of ``ops`` (the byproduct operators); the value of ``comm`` (the commutation correction, see below); and the value of ``update``, which contains the modification that must be made to the byproduct operators due to measurement outcomes.

Even though the commutation correction is computed on ``clk_r``, it must still be added to the byproduct operators on ``clk_s`` because it is not possible to update a register on two clock edges. That is the reason for using the ``comm`` register. That means that, even though the commutation correction is performed on ``clk_r``, the actual modification to the byproduct operators is still made on ``clk_s``. It is not possible to calculate the commutation correction on ``clk_s`` because of a race condition (``comm`` needs the old value of ``ops``).

The main byproduct update is performed using the entity ``ops_update``, which is a simple combinational module that combines measurement values wth program maks values to compute the update to the ``ops`` register.

The schematic of the byproduct operator calculation is shown below

.. image:: pictures/byproduct.png
  :alt: Byproduct operator update calculation
  :target: _images/byproduct.png

Commutation corrections
***********************

The commutation correction is computed using the ``comm_correct`` entity, which assigns to the output register ``comm`` on the rising edge of ``clk_r``. (``comm`` is then read on the rising edge of ``clk_s``).

Since ``comm`` is always added to ``ops``, it is necessary to set ``comm`` to zero whenever a commutation correction is not necessary.

The schematic diagram for the commutation correction is shown below:

.. image:: pictures/comm-correct.png
  :alt: Commutation correction calculation
  :target: _images/comm-correct.png

The commutation correction is the most (logically) complicated part of the design, because it uses a case statement to perform different (essentially arbitrary) operations, depending on the value of the program. 

Block design
############

The block design ``mbqc1`` for the system implementing a single logical qubit is shown below:

.. image:: pictures/block-design.png
  :alt: Block design
  :target: _images/block-design.png

The ``locked`` output indicates when the clocks are ready. The ``reset`` signal is synchronous with respect to ``clk_s`` and ``clk_r``, and resets the internal state of the RTL block. The ``enable`` signal is used to indicate the first measurement round. It should be asserted together with the rising edge of ``clk_p`` on the first measurement round of the computation.
	
The block design ``mbqc2`` for multiple qubits is not substantially different -- it uses a wider memory block for the program and uses the ``multi_qubit`` rtl block.
	
Clock management
****************

The design consists of a clock manager (MMCM) to generate the out-of-phase clocks ``clk_s`` and ``clk_r`` from ``clk_p``. The feedback input is used to lock ``clk_p`` to ``clk_p_in``, so as to satisfy external timing constraints. 

Memory management
*****************

A distributed memory generator is used to provide the program memory, and a counter is used to step through each program as the computation proceeds. The memory manager is configured as ROM in this design, essentially for simplicity (it is easy to load a coefficients file at synthesis-time that contains the program). A real design would use some kind of writeable memory that could be modified externally.

Design constraints
##################

Vivado is a constraints-driven program, meaning that it attempts to synthesize a design that meets user-specified constraints, such as input/output timing, logic placement, etc. There are two constraints files, one for the single-qubit design (``single_qubit.xdc``) and another (``multi_qubit.xdc``) for the multi-qubit design.

Timing constraints
******************

The most important constraints are input delay constraints on the measurement input and adaptive measurement setting output. (There are also timing constraints on the other inputs and outputs, but these are less important because they do not affect the critical path).

The input delays are specified using the following syntax:

.. code-block:: tcl

   set_input_delay -clock [get_clocks clk_p_in] -min -add_delay -$meas_tco_min [get_ports meas]
   set_input_delay -clock [get_clocks clk_p_in] -max -add_delay $meas_tco_max [get_ports meas]

The maximum and minimum delays from the rising edge of the clock to the rising edge of the input signal are specified using the -min and -max flags to the set_input_delay command. In the design, both these times are set to zero to indicate that the measurement input is expected to coincide with the rising edge of the clock ``clk_p_in``. 
   
The output delays are specified as follows:

.. code-block:: tcl

   set_output_delay -clock [get_clocks clk_p_in] -min -add_delay -$s_hold [get_ports s]
   set_output_delay -clock [get_clocks clk_p_in] -max -add_delay $s_setup [get_ports s]

The output signal timing characteristics are specified by providing the setup and hold times with respect to the external clock ``clk_p_in``. The setup time is the minimum amount of time the output should be valid before the rising edge of the clock. The hold time is the minimum amount of time the output should be valid after the clock rising edge.

Input/ouptut logic constraints
******************************

Registers connected to the inputs and outputs in the design can be placed in dedicated input/output logic blocks (ILOGIC and OLOGIC) in the FPGA, if they are eligible. This can improve the input/output timing characteristics of the design.

To attempt to place the register associated with a port (e.g. ``meas``) in the IOB (input/output buffers), add lines like the following to the constraints file:

.. code-block:: tcl

   set_property IOB TRUE [get_ports meas]

In the current design, only the ``meas`` port can be placed in the IOB. The adaptive measurement setting ``s`` is not eligible because it is not directly registered before the output port (there is combinational logic in the way). The ``ops`` register cannot be placed in IOB because it is routed back into the FPGA fabric.

Input/output port constraints
*****************************

In an FPGA design, it is necessary to specify the I/O specification for each port so that they are compatible with whatever system is connected to the FPGA. In the current reference design, the external analog system has not been designed, so the correct interface specification is unknown. Therefore, for the timing analysis, the default I/O specification (LVCMOS18), set to a fast slew rate as follows:

.. code-block:: tcl

   set_property SLEW FAST [get_ports s]

It is possible to set a faster I/O standard, for example HSTL_I, as follows:

.. code-block:: tcl

   set_property IOSTANDARD HSTL_I [get_ports s]

The use of higher speed I/O standards slightly improves the overall speed of the design, by reducing I/O buffer latencies. However, it would be necessary to design the external analog system in order to pick an appropriate I/O standard.


