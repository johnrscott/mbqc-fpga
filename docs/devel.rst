Todos
#####

.. todo::
   In the `Doxygen documentation <vhdldocs/index.html>`_ for the FPGA design, due to a bug in doxygen, it is not possible to view the ``adapt`` entity (404 error). I fixed this on my computer by upgrading doxygen to version 1.9.1, whereas the gitlab CI compiles the documentation using version 1.8.17. Could be fixed by figuring out how to install a later version of doxygen or using a custom runner.

License
#######

The code and data in this documentation and the associated `repository <https://gitlab.com/johnrscott/mbqc-fpga>`_ are covered by the GNU General Public License, Version 3, shown below:

.. literalinclude:: ../COPYING
