.. FPGA-MBQC documentation master file, created by
   sphinx-quickstart on Thu Jul  1 10:36:36 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MBQC-FPGA's documentation!
=====================================

This is the documentation page for the digital control system contained in the `mbqc-fpga <https://gitlab.com/johnrscott/mbqc-fpga>`_ repository.

.. warning::
    This documentation is work in progress

.. toctree::
   :maxdepth: 2
   :caption: FPGA Design

   design

.. toctree::
   :maxdepth: 2
   :caption: Functional verification

   verification

.. toctree::
   :maxdepth: 2
   :caption: Development

   devel

