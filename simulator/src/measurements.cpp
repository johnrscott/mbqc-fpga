/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * \file measurements.cpp
 * \brief Implementation of the measurments class
 *
 */

#include "measurements.hpp"

unsigned Measurements::operator() (std::size_t n) const
{
    return m[n];
}

void Measurements::pushBack(unsigned measurement)
{
    if (measurement > 1) {
	throw std::out_of_range("Measurement value must be 0 or 1");
    }
    m.push_back(measurement);
}

std::size_t Measurements::size() const {
    return m.size();
}

void Measurements::print(std::ostream & os) const {
    for (std::size_t n = 0; n < m.size() - 1; n++) {
	os << m[n] << ",";
    }
    os << m.back();
}

void Measurements::clear() {
    m.clear();
}

Bits Measurements::shiftReg(std::size_t n, std::size_t N) const
{

    if (n > m.size() - 1) {
	throw std::out_of_range("Not enough measurements for this n = "
				+ std::to_string(n));
    }
	
    ///\todo Is there a way to handle this special case better?
    if (m.size() == 0) {
	return Bits(0,N);
    }

    // Object to store the bits
    Bits reg{0, N}; // Make zero bitstring of N bits
    // If there are no measurements, return zeros
    if (m.size() != 0) {
	for (std::size_t k = 0; k < N; k++) {
	    int index = k + n + 1 - N;
	    if (index >= 0) {
		reg(k) = m[index];
	    } 
	}
    }
    return reg;
}

/// Overload for printing Measurements object to output stream
std::ostream & operator << (std::ostream & os, Measurements m)
{
    m.print(os);
    return os;
}
