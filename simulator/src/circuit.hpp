/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file circuit.hpp
 * \brief A simple file parser for reading in circuits
 *
 */

#include <fstream>

using Sim = qsl::Qubits<qsl::Type::Resize, double>;

void equalSuperposition(qsl::Qubits<qsl::Type::Resize, double> & q)
{
    for (unsigned n = 0; n < q.getNumQubits(); n++) {
	q.hadamard(n);
    }
}

void unitary(qsl::Qubits<qsl::Type::Resize, double> & q,
	     unsigned targ, double xi, double eta, double zeta)
{
    q.rotateX(targ,xi);
    q.rotateZ(targ,eta);
    q.rotateX(targ,zeta);
}

MBQC parseCircuitFile(Sim & q_correct, std::string filename)
{
    std::ifstream file{filename};

    // Check if file exists
    if (not file.good()) {
	throw std::runtime_error("Cannot open circuit file: " + filename);
    }

    // Read first line for number of qubits    
    std::string line;
    std::getline(file, line);
    std::string delimiter = "=";
    std::string token = line.substr(0, line.find(delimiter));
    if (token != "N") {
	throw std::runtime_error("Expecting N=<num_qubits> on first line");
    }

    // Get number of qubits
    unsigned nqubits;
    try {
	nqubits = std::stoi(line.erase(0,2));
    } catch (const std::invalid_argument & e) {
	throw std::runtime_error("Unable to interpret number of qubits "
				 + line);
    }

    // Check the number of qubits is not too high
    if (nqubits > 14) {
	throw std::runtime_error("This program only supports simulating "
				 "up to 14 qubits");
    }
    
    std::cout << "Number of qubits = " << nqubits << std::endl;

    // Set the correct number of qubits in the normal simulator
    // (Already got one qubit)
    for (std::size_t n = 0; n < nqubits-1; n++) {
	q_correct.appendQubit();
    }

    // Set equal superposition state
    equalSuperposition(q_correct); // Create the plus state
    
    // Create a simulator object
    MBQC mbqc(nqubits);

    // Now read the circuit contained in the file
    std::cout << "Parsing circuit file..." << std::endl;
    int line_counter = 2;
    while (std::getline(file, line))
    {
	std::istringstream iss(line);
	std::string gate;
	iss >> gate >> std::ws;
	if (gate == "cnot") {
	    unsigned c, t; // Control and target qubits
	    iss >> c >> std::ws >> t;
	    std::cout << "CNOT c=" << c << ",t=" << t << std::endl;
	    if (c >= nqubits) {
		throw std::runtime_error("Line " + std::to_string(line_counter)
					 + ": control qubit out of range");
	    }
	    if (t >= nqubits) {
		throw std::runtime_error("Line " + std::to_string(line_counter)
					 + ": target qubit out of range");
	    }
	    // Store the cnot gate
	    mbqc.controlNot(c,t);
	    // Perform the gate on the normal simulator
	    q_correct.controlNot(c,t);

	} else if(gate == "u") {
	    unsigned t; // Target qubits
	    double ax0, az1, ax2; // Angles
	    iss >> t >> std::ws
		>> ax0 >> std::ws
		>> az1 >> std::ws
		>> ax2 >> std::ws;
	    if (t >= nqubits) {
		throw std::runtime_error("Line " + std::to_string(line_counter)
					 + ": target qubit out of range");
	    }
	    std::cout << "U t=" << t
		      << ",ax=" << ax0
		      << ",az=" << az1
		      << ",ax=" << ax2
		      << std::endl;
	    
	    // Store the one-qubit unitary
	    mbqc.unitary(t,ax0,az1,ax2);
	    // Perform the gate on the normal simulator
	    unitary(q_correct,t,ax0,az1,ax2);

	    
	} else {
	    throw std::runtime_error("Line " + std::to_string(line_counter)
					 + ": Unrecognised gate " + gate);
	}
	line_counter++;
    }

    return mbqc;
}
