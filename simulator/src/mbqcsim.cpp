/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file mbqcsim.cpp
 * \brief An MBQC simulator
 *
 */


#include "mbqc.hpp"
#include "circuit.hpp"
#include "cmdline.hpp"

int main(int argc, char ** argv)
{
    CommandLine cmd;
    
    std::string program_name{ "mbqcsim" };
    std::string version{ "0.1" };
    std::string short_desc{
	"a program for simulating MBQC circuits"
    };
    std::string long_desc;
    long_desc += "mbqcsim is a program for simulating the evolution of the ";
    long_desc += "cluster state, the adaptive measurement settings and bases, ";
    long_desc += "and the byproduct operators involved in MBQC measurement ";
    long_desc += "patterns.";
    
    cmd.addOption<std::string>('c', "circuit-file",
			       "the MBQC circuit file to be simulated");
    cmd.addOption<std::string>('o', "log-file",
			       "[optional] the output simulation file basename");
    cmd.addOption<std::string>('p', "prog-file",
			       "[optional] the output program file basename");
    cmd.addOption<std::string>('s', "single-tb-file",
			       "[optional] the single qubit testbench "
			       "input file basename");
    cmd.addOption<std::string>('m', "multi-tb-file",
			       "[optional] the multi qubit testbench "
			       "input file basename");
    
    cmd.addOption<int>('q', "qubit",
		       "[optional] Specify the logical qubit to store");
    
    //cmd.setupManPageOption(program_name, short_desc, long_desc, version);
    
    if(cmd.parse(argc, argv) != 0) {
	// An error occured
	std::cerr << "An error occured while parsing the command line arguments"
		  << std::endl;
	return 1;
    }

    // Check for program mode
    std::string circuit_file = cmd.get<std::string>('c').value_or("");
    std::string log_file = cmd.get<std::string>('o').value_or("");
    std::string prog_file = cmd.get<std::string>('p').value_or("");
    std::string stb_file = cmd.get<std::string>('s').value_or("");
    std::string mtb_file = cmd.get<std::string>('m').value_or("");

    int qubit = cmd.get<int>('q').value_or(-1);
    
    // Allocate one qubit, because I don't think allocating zero
    // qubits works. Then in the parseCircuitFile function, and
    // more qubits up to the correct number
    Sim q_correct{ 1 };

    // Run the circuit from file
    if (circuit_file != "") {
	MBQC mbqc = parseCircuitFile(q_correct, circuit_file);
	Sim q = mbqc.run();

	// Check the distance between states
	double distance = qsl::fubiniStudy(q.getState(), q_correct.getState());
	if (std::abs(distance) < 1e-5) {
	    std::cout << std::endl << "Comparison with gate-based simulation "
		      << "completed successfully " << std::endl
		      << "(distance = " << distance << ")" << std::endl;
	} else {
	    std::cout << "Simulation failed "
		      << "(distance = " << distance << ")" << std::endl;
	}
	std::cout << std::endl;
	
	// Write output data
	if (log_file != "") {
	    std::cout << "Writing simulation data to file "
		      << log_file << ".log" << std::endl;

	    // Create and open a text file
	    std::ofstream file{log_file + ".log"};

	    // Print the header
	    file << "This file contains the data from each measurement round "
		 << std::endl
		 << "for every logical qubit. The format is: "
		 << std::endl << std::endl;

	    file << "Address, program (hex) (c_prog|s_prog|b_prog|theta), "
		 << "measurement, setting, byproduct ops, stored ops, angle"
		 << std::endl << std::endl;

	    if (qubit == -1) {
		for (unsigned n = 0; n < mbqc.getNumQubits(); n++) {
		    mbqc.print(n, file);
		    file << std::endl;
		}
	    } else {
		// Print the specific qubit
		mbqc.print(qubit, file);
		file << std::endl;		

	    }
	    
	    // Close the file
	    file.close();
	}

	// Write program data
	if (prog_file != "") {
	    std::cout << "Writing program data to file "
		      << prog_file << ".coe" << std::endl;
	    // Write the multi-qubit program
	    mbqc.writeCoefficients(prog_file + ".coe");
	    
	    if (qubit == -1) {
		for (unsigned n = 0; n < mbqc.getNumQubits(); n++) {
		    std::string str = prog_file + std::to_string(n) + ".coe";
		    std::cout << "Writing single qubit program coefficients file "
			      << str << " for logical qubit " << n
			      << std::endl;
		    mbqc.writeProgToFile(n, str);
		}
	    } else {
		std::string str = prog_file + std::to_string(qubit) + ".coe";
		std::cout << "Writing single qubit program coefficients file "
			  << str << " for logical qubit " << qubit
			  << std::endl;
		mbqc.writeProgToFile(qubit, str);
		
	    }
	}

	// Write single qubit testbench data
	if (stb_file != "") {
	    if (qubit == -1) {
		for (unsigned n = 0; n < mbqc.getNumQubits(); n++) {
		    std::string str = stb_file + std::to_string(n) + ".csv";
		    std::cout << "Writing single qubit testbench file "
			      << str << " for logical qubit " << n
			      << std::endl;
		    mbqc.writeToFile(n, str);
		}
	    } else {
		std::string str = stb_file + std::to_string(qubit) + ".csv";
		std::cout << "Writing single qubit testbench file "
			  << str << " for logical qubit " << qubit
			  << std::endl;
		mbqc.writeToFile(qubit, str);
		
	    }
	}

	// Write single qubit testbench data
	if (mtb_file != "") {

	    std::string str = mtb_file + ".csv";
	    std::cout << "Writing multi qubit testbench file "
		      << str << std::endl;
	    mbqc.writeToFile(str);
	 
	}

	
    } 

    return 0;

    
    
}
