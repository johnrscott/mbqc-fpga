/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file bits.cpp
 * \brief Tests for the Bits class
 *
 */
#include <gtest/gtest.h>
#include "measurements.hpp"
#include <sstream>

TEST(Measurements, MemberFunctions)
{
    // Check storing measurements works
    Measurements m;
    m.pushBack(0);
    m.pushBack(1);
    m.pushBack(1);
    m.pushBack(0);
    m.pushBack(0);
    m.pushBack(1);
    EXPECT_THROW(m.pushBack(2), std::out_of_range);
    EXPECT_EQ(m.size(), 6);
    std::stringstream ss;
    ss << m;
    EXPECT_EQ(ss.str(), "0,1,1,0,0,1");

    // Check that the shift register works
    EXPECT_EQ(m.shiftReg(0,3).val(), 0b000);
    EXPECT_EQ(m.shiftReg(1,3).val(), 0b100);
    EXPECT_EQ(m.shiftReg(2,3).val(), 0b110);
    EXPECT_EQ(m.shiftReg(3,3).val(), 0b011);
    EXPECT_EQ(m.shiftReg(4,3).val(), 0b001);
    EXPECT_EQ(m.shiftReg(5,3).val(), 0b100);
    EXPECT_THROW(m.shiftReg(6,3).val(), std::out_of_range);

    m.clear();
    EXPECT_EQ(m.size(), 0);

    // Check that an empty object returns a zero shift
    // register at index zero
    EXPECT_EQ(m.shiftReg(0,3).val(), 0b000);

}
