/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file bits.cpp
 * \brief Tests for the Bits class
 *
 */
#include <gtest/gtest.h>
#include "bits.hpp"
#include <sstream>

TEST(Bits, EmptyConstructor)
{
    // Check the default constructor
    Bits empty;
    EXPECT_EQ(empty.val(), 0);
    std::stringstream ss;
    ss << empty;
    EXPECT_EQ(ss.str(), "");
    EXPECT_EQ(empty.size(), 0);
}

TEST(Bits, ReadBits)
{
    // Do a simple test
    Bits bits{3, 3};
    std::stringstream ss;
    bits.print(ss);
    EXPECT_EQ(ss.str(), "011");
    EXPECT_EQ(bits.val(), 3);
    EXPECT_EQ(bits(0), 1);
    EXPECT_EQ(bits(1), 1);
    EXPECT_EQ(bits(2), 0);
    EXPECT_THROW(bits(3), std::out_of_range);
}

TEST(Bits, ConstReadBits)
{
    // Check the const context read exception
    const Bits const_bits{0b101011011, 5};
    EXPECT_EQ(const_bits(0), 1);
    EXPECT_EQ(const_bits(2), 0);
    EXPECT_THROW(const_bits(10), std::out_of_range);
}

TEST(Bits, ModifyBits)
{
    // Check that the bits can be modified
    Bits bits{0b011, 3};
    bits(2) = 1;
    std::stringstream ss;//ss.str(std::string());
    ss << bits;
    EXPECT_EQ(ss.str(), "111");
}

TEST(Bits, XorEquals)
{
    // Test the xor equals
    Bits more_bits(0b101, 3);
    Bits bits(0b111, 3);
    bits ^= more_bits;
    EXPECT_EQ(bits.val(), 0b010);

    // Test xor equals with literal
    EXPECT_EQ((bits ^= 0b010).val(), 0b000);

    // Test xor operation between two bitstrings
    Bits a{0b10101, 5};
    Bits b{0b11000, 5};
    EXPECT_EQ((a ^ b).val(), 0b01101);
}

TEST(Bits, Concaternation)
{
    // Check concaternation of two bitstrings
    EXPECT_EQ((Bits(0b111, 3) + Bits(0b01, 2) + Bits(0b0, 1)).val(), 0b111010);
}

TEST(Bits, SetBits)
{
    // Check assignment operator
    Bits a;
    a.set(0b00100, 5);
    EXPECT_EQ(a.val(), 0b00100);
}

TEST(Bits, XorAll)
{
    // Check that xoring all the bits together works
    EXPECT_EQ(Bits(0b10101, 6).xorBits(), 1);
    EXPECT_EQ(Bits(0b10100, 6).xorBits(), 0);
    EXPECT_EQ(Bits(0b1, 1).xorBits(), 1);
}

TEST(Bits, Substring)
{
    // Check that substring function works
    Bits c{ 0b1100101101, 10 };
    EXPECT_EQ((c(4,0).val()), 0b01101);
}

