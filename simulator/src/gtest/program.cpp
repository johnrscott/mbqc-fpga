/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file bits.cpp
 * \brief Tests for the Bits class
 *
 */
#include <gtest/gtest.h>
#include "program.hpp"
#include <sstream>

TEST(Program, TwoArgConstructor)
{
    // Check that the two argument constructor works
    Program a{1.2, 0b1, 0b11};
    std::stringstream ss;
    ss << a;
    EXPECT_EQ(ss.str(), "00000|00001|000011|1.2");
}

TEST(Program, Getters)
{
    Program a{1.2, 0b1, 0b11};

    // Check that the getters work
    EXPECT_FLOAT_EQ(a.angle(), 1.2);
    EXPECT_EQ(a.b_prog().val(), 0b11);
    EXPECT_EQ(a.s_prog().val(), 0b1);
    EXPECT_EQ(a.c_prog().val(), 0b0);
}

TEST(Program, ThreeArgConstructor)
{
  
    // Check that the three argument constructor works
    Program b{1.2, 0b1, 0b11, 0b111};
    std::stringstream ss;//ss.str(std::string()); // Clear stream
    b.print(ss);
    EXPECT_EQ(ss.str(), "00111|00001|000011|1.2");
}

TEST(Program, SetCProg)
{
    // Check that setting the c_prog works
    Program b{0, 0b1, 0b1};
    EXPECT_EQ(b.c_prog().val(), 0);
    b.setc_prog(0b111);
    EXPECT_EQ(b.c_prog().val(), 0b111);

}

TEST(Program, ConstructorExceptions)
{
    // Check that the constructors throw exceptions
    EXPECT_THROW((Program{1.2, 0b111111, 0b0}), std::out_of_range);
    EXPECT_THROW((Program{1.2, 0b0, 0b1111111}), std::out_of_range);
    EXPECT_THROW((Program{1.2, 0b0, 0b0, 0b111111}), std::out_of_range);
}

TEST(Program, ByproductCalculation)
{
    // Test
    Program a{1.2, 0b0, 0b010101};
    EXPECT_EQ(a.bypCorrection({0b000,3}).val(), 0b00);
    EXPECT_EQ(a.bypCorrection({0b001,3}).val(), 0b01);
    EXPECT_EQ(a.bypCorrection({0b010,3}).val(), 0b10);
    EXPECT_EQ(a.bypCorrection({0b110,3}).val(), 0b11);

    Program b{1.2, 0b0, 0b111000};
    EXPECT_EQ(b.bypCorrection({0b000,3}).val(), 0b00);
    EXPECT_EQ(b.bypCorrection({0b001,3}).val(), 0b10);
    EXPECT_EQ(b.bypCorrection({0b010,3}).val(), 0b10);
    EXPECT_EQ(b.bypCorrection({0b110,3}).val(), 0b00);

}

TEST(Program, MeasurementSettingCalculation)
{
    Program a{1.2, 0b01111, 0b01};
    EXPECT_EQ(a.measurementSetting({0b000,3}, {0b00,2}), 0);
    EXPECT_EQ(a.measurementSetting({0b001,3}, {0b00,2}), 1);
    EXPECT_EQ(a.measurementSetting({0b010,3}, {0b01,2}), 0);
    EXPECT_EQ(a.measurementSetting({0b100,3}, {0b01,2}), 0);

    Program b{1.2, 0b11010, 0b0};
    EXPECT_EQ(b.measurementSetting({0b000,3}, {0b01,2}), 1);
    EXPECT_EQ(b.measurementSetting({0b011,3}, {0b10,2}), 0);
    EXPECT_EQ(b.measurementSetting({0b111,3}, {0b00,2}), 1);
    EXPECT_EQ(b.measurementSetting({0b100,3}, {0b11,2}), 0);

    
    
}
