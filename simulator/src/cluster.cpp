/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file cluster.cpp
 * \brief Implementation of the cluster state simulator
 *
 */

#include <qsl/qubits.hpp>
#include <qsl/utils/quantum.hpp>
#include <qsl/utils/random.hpp>
#include "cluster.hpp"

/**
 * \brief Measure the end qubit in a particular basis and remove it
 *
 * \param phi The measurement angle
 * \param s The measurement setting, which adds a sign to the angle
 */
unsigned measure(qsl::Qubits<qsl::Type::Resize, double> & q,
		 double phi, unsigned s)
{
    
    // Measure and remove qubit 0 in a basis depending on phi and s
    if (s == 0) {
	q.rotateZ(0, -phi+M_PI/2); ///\todo replace with Z-rotation and change sign
    } else if (s == 1) {
	q.rotateZ(0, phi+M_PI/2); ///\todo replace with Z-rotation and change sign
    } else {
	throw std::logic_error("Unrecognised measurement setting");
    }
    q.rotateX(0, M_PI/2); // X-rotation
    return q.measureOut(0); // Perform the measurement
}

ClusterState::ClusterState(unsigned N, const std::vector<bool> & entangle)
    : N{N}, D{1}, q{D*N}
{
    // Check that the entangle vector is the right length
    if (entangle.size() != N-1) {
	throw std::logic_error("Entangle vector must have length N-1");
    }
	    
    // Make the equal superposition state
    for (std::size_t k = 0; k < D*N; k++) {
	q.hadamard(k);
    }

    // Apply CZ between vertical neighbours according to entangle
    for (std::size_t k = 0; k < N-1; k++) {
	if (entangle[k] == true) {
	    std::size_t index1 = k; // column 0, row k
	    std::size_t index2 = k + 1; // column, row k+1
	    q.controlZ(index1,index2); // CZ for column d
	}
    }
}

std::vector<unsigned> ClusterState::measureColumn(std::vector<double> angles,
						  std::vector<unsigned> s)
{
    if (angles.size() != N) {
	throw std::logic_error("Column height must be N");
    }

    if (D == 1) {
	throw std::logic_error("You cannot measure out the final column");
    }

    std::vector<unsigned> outcomes;
    for (std::size_t k = 0; k < N; k++) {
	outcomes.push_back(measure(q,angles[k],s[k]));

    }

    // Reduce the depth variable by one
    D--;
	    
    return outcomes;	    
}

qsl::Qubits<qsl::Type::Resize, double> ClusterState::getQubits() const
{

    if (D != 1) {
	throw std::logic_error("You must measure all but one column first");
    }
	
    return q;
}

void ClusterState::addColumn(const std::vector<bool> & entangle)
{
    // Check that the entangle vector is the right length
    if (entangle.size() != N-1) {
	throw std::logic_error("Entangle vector must have length N-1");
    }
	    
    // Add the new qubits to the statevector
    for (std::size_t k = 0; k < N; k++) {
	q.appendQubit();
    }

    // Increment the column depth
    D++;
	    
    // Prepare the new qubits in the equal superposition
    for (std::size_t k = (D-1)*N; k < D*N; k++) {
	q.hadamard(k);
    }

    // Apply CZ between vertical neighbours according to entangle
    for (std::size_t k = 0; k < N-1; k++) {
	for (std::size_t d = D-1; d < D; d++) {
	    if (entangle[k] == true) {
		std::size_t index1 = d*N + k; // Column d, row k
		std::size_t index2 = d*N + k + 1; // Column d, row k+1
		q.controlZ(index1,index2); // CZ for column d
	    }
	}
    }

    // Apply CZ between all horizontal neighbours
    for (std::size_t k = 0; k < N; k++) {
	std::size_t index1 = (D-1)*N + k; // Column D-1, row k
	std::size_t index2 = (D-2)*N + k; // Column D-2, row k
	q.controlZ(index1,index2); // CZ for column d		
    }
}
