/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef PROGRAM_HPP
#define PROGRAM_HPP

/**
 * \file program.hpp
 * \brief Contains a class for storing the program word
 *
 */

#include "bits.hpp"
#include <iomanip>

/**
 * \brief Class for storing the program
 *
 * This class stores the program for a single measurement round of one
 * logical qubit. This comprises the measurement angle, the method
 * to compute the next measurement setting, the method to obtain the
 * byproduct operators, and then any other corrections that need to
 * be performed.
 *
 */
class Program
{
    double phi; ///< The measurement angle

    /// Program for computing the byproduct operators
    Bits b_prog_bits;
    
    /// Program for computing the adaptive measurement setting
    Bits s_prog_bits;

    /// Program for computing corrections
    Bits c_prog_bits;
    
public:

    /**
     * \brief Construct a program object
     *
     * This constructor makes a program object containing the adaptive
     * measurement setting specification s_prog, and the byproduct 
     * operator specification b_prog (the commutation correction is
     * set to zero). It also includes the adaptive measurement setting angle
     * theta
     *
     */
    Program(double theta, unsigned s_prog, unsigned b_prog);

    /**
     * \brief Construct a program including the commutation correction
     *
     * This constructor is used when the commutation correction specification
     * c_prog is also known. The behaviour otherwise is the same as the 
     * other constructor
     */
    Program(double theta, unsigned s_prog, unsigned b_prog, unsigned c_prog);

    /**
     * \brief Print the program object
     *
     * The output contains separators to show where the different
     * parts of the program word start. The format is:
     *
     * Type:  5 bits | 5 bits | 6 bits | double
     * Value: c_prog | s_prog | b_prog | angle
     *
     */ 
    void print(std::ostream & os) const;

    /**
     * \brief Return the program word as an integer
     */
    int getProgramWord() const;

    /**
     * \brief Set the commutation correction specification c_prog
     */
    void setc_prog(unsigned c_prog);
    
    /** Get the adaptive part of the instruction
     *
     * \return A standard vector containing the adaptive part of the
     * instruction. The vectors contains the bits of the instruction,
     * accessible via their bit location (higher index means more
     * significant).
     * 
     * The low 3 bits are used as a mask with the shift register of
     * measurements to determine the measurement contribution to s.
     * The high two bits are used as a mask for the stored byproduct
     * operators.
     *
     */
    Bits s_prog() const;

    /** Get the byproduct part of the instruction
     *
     * \return A standard vector containing the byproduct part of the
     * instruction (not containing the commutation correction.
     *
     * The low 3 bits are used as a mask with the measurements for the
     * current and neighbouring qubits for calculating the z part 
     * of the byproduct operator. The high 3 bits are used to calculate
     * the z part. 
     */
    Bits b_prog() const;

    /** Get the commutation correction part
     *
     * \return A standard vector containing the comm part of the
     * instruction
     *
     * This part of the program controls the commutation correction,
     * the stored byproduct operators, and the addition of ones
     * into the byproduct operator. It comprises 5 bits, as follows:
     *
     * comm[0]: If high then store the byproduct operators
     * comm[1]: If high then a commutation correction is necessary, in which case
     *   comm[2]: If high then current logical qubit is the control
     *   comm[3]: If high then other qubit in CNOT is above
     * comm[4]: If high then add ones to the byproduct operators, in which case
     *   comm[2]: Value to add to z
     *   comm[3]: value to add to x
     * 
     */
    Bits c_prog() const;
    
    /**
     * \brief Get the measurement angle
     */
    double angle() const;

    /**
     * \brief Compute the correction to the byproduct operators
     *
     * This function returns the two bit correction to the byproduct
     * operators, which should be xor-ed with the current byproduct
     * operators to update them.
     *
     * \param b_prog The program which controls how to update the byproduct
     * operators from the last round of measurements.
     * 
     * \param meas The most recent measurement outcomes from the current
     * and neighbouring qubits. The middle bit meas[1] is from the current
     * qubit, m[0] is from the qubit below and m[2] is from the qubit above.
     *
     */
    Bits bypCorrection(const Bits & meas);

    /**
     * \brief Compute the measurement setting s
     *
     * \param s_prog The program which determines how the setting is computed
     * \param shift_reg The past measurement values (length 3)
     * \param ops_stored The stored byproduct operators
     *
     */
    unsigned measurementSetting(const Bits & shift_reg,
				const Bits & ops_stored);
    
    /**
     * \brief Compute the commutation correction, store byproducts, etc
     *
     * This function updates the stored byproduct operators, performs
     * a commutation correction, or adds a constant to the byproduct
     * operators depending on the comm part of the program.
     *
     * It should be called after the storeOutcome function has been called,
     * because it used the current byproduct operators from the qubits above
     * and below
     *
     * In the hardware, this function is computed on clk_r, after the
     * byproduct operators and measurement settings have been computed
     * on clk_s. The value of the stored byproduct operators will be
     * available for the next measurement round.
     *
     * \return A pair of two parameters. The first is a Bits object, 
     * containing the correction that should be applied to the
     * byproduct operator. The second is a bool which indicates
     * whether the byproduct operators should be stored before
     * applying the correction.
     *
     */
    std::pair<Bits,bool> commCorrect(const Bits & ops_above,
				     const Bits & ops_below);
};


/// Overload Program printing for ostream
std::ostream & operator << (std::ostream & os, const Program & p);
#endif
