/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file program.cpp
 * \brief Implementation of the program class
 */

#include "program.hpp"
#include <iomanip>


Program::Program(double phi, unsigned s_prog, unsigned b_prog)
    : Program(phi, s_prog, b_prog, 0)
{
}

Program::Program(double phi_in, unsigned s_prog, unsigned b_prog, unsigned c_prog)
    : phi(phi_in)
{
    if (s_prog > 0b11111) {
	throw std::out_of_range("s_prog is out of range, only use 5 bits");
    }
    if (b_prog > 0b111111) {
	throw std::out_of_range("b_prog is out of range, only use 6 bits");
    }

    if (c_prog > 0b11111) {
	throw std::out_of_range("c_prog is out of range, only use 5 bits");
    }

    b_prog_bits.set(b_prog, 6);
    s_prog_bits.set(s_prog, 5);
    c_prog_bits.set(c_prog, 5);
}

void Program::print(std::ostream & os) const
{
    std::ios_base::fmtflags f{ os.flags() };
    os << c_prog_bits << "|" << s_prog_bits << "|"
       << b_prog_bits << "|" << std::fixed << phi;
    os.flags(f);
}

int Program::getProgramWord() const
{
    return (c_prog_bits.val() << 11) | (s_prog_bits.val() << 6)
	| (b_prog_bits.val());
}
    
void Program::setc_prog(unsigned c_prog)
{
    c_prog_bits.set(c_prog, 5);
}

Bits Program::s_prog() const
{
    return s_prog_bits;
}

Bits Program::b_prog() const
{
    return b_prog_bits;
}

Bits Program::c_prog() const
{
    return c_prog_bits;
}

double Program::angle() const
{
    return phi;
}

Bits Program::bypCorrection(const Bits & meas)
{
    if (meas.size() != 3) {
	std::logic_error("Expecting meas to have size == 3 "
			 "in bypCorrection");
    }
	    
    Bits bits{0b00,2};
    bits(0) = (meas & b_prog_bits(2,0)).xorBits(); // z
    bits(1) = (meas & b_prog_bits(5,3)).xorBits(); // x
    return bits;
}

unsigned Program::measurementSetting(const Bits & shift_reg,
				     const Bits & ops_stored)
{
    if (shift_reg.size() != 3) {
	std::logic_error("Expecting shift_reg to have size == 3 "
			 "in measurementSetting");
    }

    if (ops_stored.size() != 2) {
	std::logic_error("Expecting ops_stored to have size == 2 "
			 "in measurementSetting");
    }

    unsigned s = 0;

    // Compute the part due to measurements
    s ^= (s_prog_bits(2,0) & shift_reg).xorBits();

    // Compute the part due to byproduct operators
    s ^= (s_prog_bits(4,3) & ops_stored).xorBits();

    return s;
}

std::pair<Bits,bool> Program::commCorrect(const Bits & ops_above,
					  const Bits & ops_below)
{	    
    // Indicate whether the operators should be stored
    bool update_flag = false;
    if (c_prog_bits(0) == 1) {
	update_flag = true;
    }

    // Byproduct operator correction
    Bits correction{0b00,2};
	    
    // Add constants to the byproduct operators if necessary
    if (c_prog_bits(4) == 1) {
	correction = c_prog_bits(3,2);
    } else if (c_prog_bits(1) == 1) {
	// Perform the commutation correction if necessary
	if (c_prog_bits(3,2).val() == 0b11) {
	    // Current qubit is control, target above
	    correction(0) = ops_above(0);
	}
	else if (c_prog_bits(3,2).val() == 0b10) {
	    // Current qubit is target, control above
	    correction(1) = ops_above(1);
	}
	else if (c_prog_bits(3,2).val() == 0b01) {
	    // Current qubit is control, target below
	    correction(0) = ops_below(0);
	}
	else if (c_prog_bits(3,2).val() == 0b00) {
	    // Current qubit is target, control below
	    correction(1) = ops_below(1);
	}
    }

    return {correction, update_flag};
}

/// Overload Program printing for ostream
std::ostream & operator << (std::ostream & os, const Program & p)
{
    p.print(os);
    return os;
}
