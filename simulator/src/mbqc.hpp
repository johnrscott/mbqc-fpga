/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file mbqc.hpp
 * \brief Utilities for simulating measurement based quantum computing
 *
 */

#ifndef MBQC_HPP
#define MBQC_HPP

#include <iostream>
#include "cluster.hpp"
#include "qubit.hpp"

/**
 * \brief Measurement-based quantum computing (MBQC) simulator
 *
 * This class is an MBQC simulator, capable of simulating up to about
 * 14 logical qubit rows on a regular laptop, that can implement
 * the measurement patterns for arbitrary one-qubit gates and CNOT
 * gates. The purpose of the class is to verify measurment patterns,
 * and generate output data that can be used by VHDL testbenches to
 * verify the design of an FPGA implementation of photonic MBQC.
 *
 */
class MBQC
{
    const unsigned N; ///< The number of logical qubits
    ClusterState cluster; ///< Cluster state simulator
    std::vector<LogicalQubit> qubits; ///< The set of logical qubits
    
    /**
     * \brief Apply an arbitrary one-qubit gate to a logical qubit
     *
     * \param index Which qubit to apply the gate to. 
     */
    void addUnitary(unsigned targ, double xi, double eta, double zeta);

    /**
     * \brief Add an identity gate to the logical qubit
     *
     * This gate is length two and is used to pad the logical qubit
     * program so it is the same length as the surrounding logical
     * qubits.
     * 
     * \param targ Which qubit to apply the identity to. 
     */
    void addIdentity(unsigned targ);
    
    /**
     * \brief Add a Hadamard gate
     *
     * \param targ Which qubit to apply the Hadamard to. 
     *
     * \todo This is currently missing the commutation correction
     */
    void addHadamard(unsigned targ);
    
    /**
     * \brief Add a CNOT gate to a pair of logical qubits
     *
     */
    void addControlNot(unsigned ctrl, unsigned targ);

    /**
     * \brief Add a program word to one of the qubits
     *
     * This function is used to push a program to a particular logical
     * qubit. The program comprises the 
     *
     * Use this function to add the entanglement structure for the current
     * logical qubit. The options are Entangle::None, if the current cluster 
     * qubit should not be entangled with a vertical neighbour, or 
     * Entangle::Below, if the qubit should be entangled with the qubit
     * below. Each qubit is only responsible for entanglement to the qubit
     * below (if there is entanglement above, that is recorded in the qubit
     * above).
     * 
     */
    void pushProgram(std::size_t n, const Program & prog, Entangle entangle);
    
    /**
     * \brief Add a commutation correction to the last instruction
     *
     */     
    void addComm(std::size_t n, unsigned comm);

public:

    /// Return the number of logical qubits
    unsigned getNumQubits() const;
    
    /**
     * \brief Construct an MBQC object with N logical qubits
     */
    MBQC(unsigned N);

    /// Print the nth logical qubit to the ostream os
    void print(unsigned n, std::ostream & os);

    /**
     * \brief Write simulated data for qubit n to a file
     *
     * This function was the original testbench input, that has
     * now been replaced by other functions.
     *
     * \todo Refactor this with the new functions 
     */
    void writeToFile(unsigned n, std::string filename);

    /**
     * \brief Write data from all qubits to a file
     *
     * Use this function to write the measurement outcomes,
     * measurement settings, and byproduct operators from all
     * the qubits to a file. The output is used in the testbench
     * for mbqc2 (multi_qubit)
     *
     * The measurement results are stored as a bitstring where
     * the least significant bit corresponds to the topmost
     * logical qubit. The ops are stored as a flat bitstring
     * where each pair of bits is the byproduct operators for
     * a particular logical qubit. The least significant two bits
     * corresponds to the topmost qubit.
     *
     * This function does not store the program. For that, use
     * writeCoefficients.
     *
     */
    void writeToFile(const std::string & filename);

    /**
     * \brief Write simulation data for logical qubit n to file
     *
     * Another variant of the write to file function
     *
     */
    void writeReadableFile(unsigned n, std::string filename);

    /**
     * \brief The function outputs a latex formatted table of data
     */
    void writeTableFile(std::string filename);
    

    /**
     * \brief Write the program for qubit n to a coefficients file
     *
     * This functions outputs the program for the nth logical qubit
     * to a coefficient file (.coe) that can be loaded into the
     * Xilinx distributed memory generator IP in the initialisation
     * section.
     *
     */
    void writeProgToFile(unsigned n, const std::string & filename);

    /**
     * \brief Write the program for all qubits to a coefficients file
     *
     * Use this function to write the coefficients file for all logical
     * qubits (for the multi-qubit case) to a file 
     */
    void writeCoefficients(const std::string & filename);

    /**
     * \brief Run the MBQC simulation
     *
     * Call this function after setting the program to run MBQC circuit
     * and compute measurement outcomes, adaptive measurement settings,
     * and byproduct operators for each measurement round. The simulation
     * is performed by simulating a cluster state of the right entanglement
     * shape and making the single-qubit measurements of the measurement pattern.
     *
     * At the end of the computation, the final column of the cluster state
     * is returned so that is can be compared with a gate-based simulation
     * of the same circuit to verify its correctness.
     *
     */
    qsl::Qubits<qsl::Type::Resize, double> run();
    
    /**
     * \brief Add a one-qubit unitary to the MBQC circuit
     *
     * Use this function to add a one-qubit gate to the MBQC circuit. The
     * parameters of the one-qubit gate are specified as the Euler angles
     * xi, eta and zeta. The gate that is realised is
     *
     * \f[
     * U = R_X(\zeta)R_Z(\eta)R_X(\xi)
     * \f] 
     *
     * \param targ The logical qubit on which to apply the one-qubit gate
     * \param xi The first X-rotation 
     * \param eta A subsequent Z-rotation
     * \param zeta The final X-rotation
     *
     */
    void unitary(unsigned targ, double xi, double eta, double zeta);

    /**
     * \brief Add a Hadamard gate to the circuit
     */
    void hadamard(unsigned targ);
    
    /**
     * \brief Add a CNOT gate to the circuit
     *
     * This function adds a CNOT gate between two adjacent logical qubits
     * specified by the arguments.
     *
     * \param ctrl The control qubit index
     * \param targ The target qubit index
     *
     */
    void controlNot(unsigned ctrl, unsigned targ);
    
};

#endif
