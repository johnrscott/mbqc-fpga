/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file proggen.hpp
 * \brief A program for generating program for an arbitrary number of qubits
 *
 */

#include <iostream>
//#include "cluster.hpp"
#include "qubit.hpp"
#include <cmath>
#include <fstream>

#define X_BASIS 0
#define Y_BASIS (M_PI/2)

/**
 *
 *
 */
class ProgGen
{
    const unsigned N; ///< The number of logical qubits
    std::vector<LogicalQubit> qubits; ///< The set of logical qubits
    
    /**
     * \brief Apply an arbitrary one-qubit gate to a logical qubit
     *
     * \param index Which qubit to apply the gate to. 
     */
    void addUnitary(unsigned targ, double xi, double eta, double zeta)
	{
	    // Add the instruction to store the byproduct operators
	    addComm(targ, 0b00001);  
    
	    pushProgram(targ, {0,     0b01100, 0b000010}, Entangle::None);
	    pushProgram(targ, {-xi,   0b10100, 0b010000}, Entangle::None);
	    pushProgram(targ, {-eta,  0b01101, 0b000010}, Entangle::None);
	    pushProgram(targ, {-zeta, 0b00000, 0b010000}, Entangle::None);
	}

    /**
     * \brief Add an identity gate to the logical qubit
     *
     * This gate is length two and is used to pad the logical qubit
     * program so it is the same length as the surrounding logical
     * qubits.
     * 
     * \param targ Which qubit to apply the identity to. 
     */
    void addIdentity(unsigned targ)
	{
	    pushProgram(targ, {0, 0b00000, 0b000010}, Entangle::None);
	    pushProgram(targ, {0, 0b00000, 0b010000}, Entangle::None);
	}

    /**
     * \brief Add a Hadamard gate
     *
     * \param targ Which qubit to apply the Hadamard to. 
     *
     * \todo This is currently missing the commutation correction
     */
    void addHadamard(unsigned targ)
	{
	    pushProgram(targ, {X_BASIS, 0b00000, 0b010000}, Entangle::None);
	    pushProgram(targ, {Y_BASIS, 0b00000, 0b000010}, Entangle::None);
	    pushProgram(targ, {Y_BASIS, 0b00000, 0b010000}, Entangle::None);
	    pushProgram(targ, {Y_BASIS, 0b00000, 0b010010}, Entangle::None);
	}

    
    /**
     * \brief Add a CNOT gate to a pair of logical qubits
     *
     */
    void addControlNot(unsigned ctrl, unsigned targ)
	{
	    // Add the commutation correction to last program word
	    addComm(ctrl, 0b00110);
	    addComm(targ, 0b01010);
    
	    // Control qubit
	    pushProgram(ctrl, {X_BASIS, 0b00000, 0b000011},
			Entangle::None); // 0
	    pushProgram(ctrl, {Y_BASIS, 0b00000, 0b010000}, Entangle::None); // 1
	    pushProgram(ctrl, {Y_BASIS, 0b00000, 0b010011,
				    0b10100}, Entangle::None); // 2
	    pushProgram(ctrl, {X_BASIS, 0b00000, 0b000010}, Entangle::Below); // 3
	    pushProgram(ctrl, {Y_BASIS, 0b00000, 0b010010}, Entangle::None); // 4
	    pushProgram(ctrl, {Y_BASIS, 0b00000, 0b010000}, Entangle::None); // 5

	    // Target qubit
	    pushProgram(targ, {X_BASIS, 0b00000, 0b000010}, Entangle::None); // 7
	    pushProgram(targ, {X_BASIS, 0b00000, 0b110000}, Entangle::None); // 8
	    pushProgram(targ, {X_BASIS, 0b00000, 0b100010}, Entangle::None); // 9
	    pushProgram(targ, {X_BASIS, 0b00000, 0b010000}, Entangle::None); // 10
	    pushProgram(targ, {X_BASIS, 0b00000, 0b000010}, Entangle::None); // 11
	    pushProgram(targ, {X_BASIS, 0b00000, 0b010000}, Entangle::None); // 12
	}

    /**
     * \brief Add a program word to one of the qubits
     *
     * This function is used to push a program to a particular logical
     * qubit. The program comprises the 
     *
     * Use this function to add the entanglement structure for the current
     * logical qubit. The options are Entangle::None, if the current cluster 
     * qubit should not be entangled with a vertical neighbour, or 
     * Entangle::Below, if the qubit should be entangled with the qubit
     * below. Each qubit is only responsible for entanglement to the qubit
     * below (if there is entanglement above, that is recorded in the qubit
     * above).
     * 
     */
    void pushProgram(std::size_t n, const Program & prog, Entangle entangle)
	{
	    // Check that n is in range
	    if (n >= qubits.size()) {
		throw std::out_of_range("n is out of range in pushProgram");
	    }
	    qubits[n].pushProgram(prog, entangle);
	}

    /**
     * \brief Add a commutation correction to the last instruction
     *
     */     
    void addComm(std::size_t n, unsigned comm)
	{
	    qubits[n].addComm(comm);
	}
    


public:
    ProgGen(unsigned N)
	: N{N}, qubits(N)
	{ }


    void print() const
	{
	    // Loop over rounds
	    for (std::size_t k = 0; k < qubits[0].size(); k++) {

		// Loop over qubits
		for (std::size_t n = 0; n < N; n++) {
		    
		    // Get the kth program
		    Program prog{ qubits[n].getProgram(k) };
		
		    // Write the program word
		    std::ios_base::fmtflags f( std::cout.flags() );
		    std::cout << std::hex << std::setw(4) << std::setfill('0')
			      << prog.getProgramWord() << ",";
		    std::cout.flags( f );
		}
		std::cout << std::endl;
	    }
	}
    
    void writeProgToFile(unsigned n, const std::string & filename)
	{
	    // Create and open a text file
	    std::ofstream file{filename};

	    // Write the header row
	    file << "memory_initialization_radix=2;" << std::endl;
	    file << "memory_initialization_vector=";

	    // Write the data
	    for (std::size_t k = 0; k < qubits[n].size(); k++) {

		// Get the kth program
		Program prog{ qubits[n].getProgram(k) };
		
		file << prog.c_prog(); // Commutation correction
		file << prog.s_prog(); // Adaptive measurement
		file << prog.b_prog(); // Byproduct computation
		file << " "; // Write space separator
	    }

	    
	    // Close the file
	    file.close();
	}

    /// Write the program for all qubits to a file
    void writeCoefficients(const std::string & filename)
	{
	    // Create and open a text file
	    std::ofstream file{filename};

	    // Write the header row
	    file << "memory_initialization_radix=16;" << std::endl;
	    file << "memory_initialization_vector=";

	    // Loop over the program counter
	    for (std::size_t k = 0; k < qubits[0].size(); k++) {

		// Loop over the qubits
		// The qubit program words must be printed in reverse
		// order for the coefficients file so that the program
		// word for qubit 0 is the least significant word
		for (int n = N-1; n >= 0; n--) {
		    
		    // Get the kth program
		    Program prog{ qubits[n].getProgram(k) };

		    // Write the program word
		    std::ios_base::fmtflags f( file.flags() );
		    file << std::hex << std::setw(4) << std::setfill('0')
			 << prog.getProgramWord();
		    file.flags( f );
		}
		file << std::endl; // Write newline separator
	    }
		
	    // Close the file
	    file.close();
	}

    /**
     * \brief Add a one-qubit unitary to the circuit
     */
    void unitary(unsigned targ, double xi, double eta, double zeta)
	{
	    // There is no need to check the length of the other qubit
	    // programs here, just push back the unitary
	    addUnitary(targ, xi, eta, zeta);
	}

    /**
     * \brief Add a Hadamard gate to the circuit
     */
    void hadamard(unsigned targ)
	{
	    // There is no need to check the length of the other qubit
	    // programs here, just push back the unitary
	    addHadamard(targ);
	}

    /**
     * \brief Add a CNOT gate to the circuit
     *
     */
    void controlNot(unsigned ctrl, unsigned targ)
	{
	    // Check that both the ctrl and targ qubits have programs
	    // of equal length. If not, push back identities to make
	    // the lengths equal
	    const std::size_t ctrl_len{ qubits[ctrl].size() };
	    const std::size_t targ_len{ qubits[targ].size() };
	    if (ctrl_len != targ_len) {
		// First check that the lengths differ by a multiple
		// of two, so they can be corrected by identity gates
		if (((ctrl_len - targ_len) % 2) == 0) {
		    // Pad the shorter qubit with identities
		    if (ctrl_len < targ_len) {
			while (qubits[ctrl].size() < targ_len) {
			    addIdentity(ctrl);
			}
		    } else {
			while (qubits[targ].size() < ctrl_len) {
			    addIdentity(targ);
			}
		    }
		} else {
		    throw std::runtime_error("Unable to correct using identity");
		}
	    }

	    // Now add the CNOT gate
	    addControlNot(ctrl,targ);
	}

    /// Ensure that each qubit program is the same length
    void padProgram()
	{

	    // First equalise the length of all the qubits
	    std::size_t max_len = 0;
	    for (std::size_t n = 0; n < qubits.size(); n++) {
		max_len = std::max(max_len, qubits[n].size());
	    }

	    // Now pad all the qubits to the same length
	    for (std::size_t n = 0; n < qubits.size(); n++) {
		while(qubits[n].size() < max_len) {
		    addIdentity(n);
		}
	    }
	
	    // Check that everything is the right length
	    std::size_t length = qubits[0].size(); ///< Program length
	    for (std::size_t n = 1; n < qubits.size(); n++) {
		if (qubits[n].size() != length) {
		    throw std::runtime_error("All the qubits must have the same "
					     "length before run() is called.");
		}
	    }
	}
    
};

int main()
{
    unsigned num_qubits{64}; 
    unsigned rounds{5};
    
    ProgGen prog{num_qubits};

    // Repeat several times
    for (std::size_t k = 0; k < rounds; k++) {	
	// Apply single qubit gates to all qubits
	for (std::size_t n = 0; n < num_qubits; n++) {
	    prog.unitary(n,0.1,0.2,0.3);
	}
	// Apply entangling gates between all qubits
	for (std::size_t n = 0; n < num_qubits-1; n++) {
	    prog.controlNot(n,n+1);
	}
    }

    // Equalise the program lengths
    prog.padProgram();

    // Print tos console
    prog.print();

    // Write coefficients file
    prog.writeCoefficients("prog64.coe");

}
