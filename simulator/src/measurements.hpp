/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef MEASUREMENTS_HPP
#define MEASUREMENTS_HPP

/**
 * \file measurements.hpp
 * \brief Contains a class for holding and manipulating measurement history
 *
 */

#include "bits.hpp"

/**
 * \brief Class for holding measurement history
 */
class Measurements
{
    std::vector<unsigned> m; ///< Measurement results
    
public:

    // Get the nth measurement read only
    unsigned operator() (std::size_t n) const;
    
    /// Add a measurement
    void pushBack(unsigned measurement);
    
    /// Get the number of measurements
    std::size_t size() const;
    
    /// Print the measurements object
    void print(std::ostream & os) const;
    
    /// Clear the measurement history
    void clear();
    
    /**
     * \brief Return the shift register of past measured outcomes
     *
     * \param n The index of the most recent measurement round to
     * include
     * \param N The number of elements to include in the shift
     * register. If there are not enough measurement rounds then
     * the register will be padded with zeros.
     *
     * \return A Bits object containing the past measurement
     * outcomes. The most recent measurement is contained at
     * the highest index
     *
     */
    Bits shiftReg(std::size_t n, std::size_t N) const;
};

/// Overload for printing Measurements object to output stream
std::ostream & operator << (std::ostream & os, Measurements m);

#endif
