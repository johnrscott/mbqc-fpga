/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file cluster.hpp
 * \brief A recycling cluster state for simulating MBQC
 *
 */

#ifndef CLUSTER_HPP
#define CLUSTER_HPP

#include <qsl/qubits.hpp>
#include <qsl/utils/quantum.hpp>
#include <qsl/utils/random.hpp>

/**
 * \brief A recycling cluster state simulator
 *
 * This class is a rectangular cluster state simulator with the 
 * ability to re-allocate measured columns of qubits on the 
 * other side of the grid. This makes it possible to simulate
 * arbitrary depth cluster state circuits containing N logical
 * qubits using a statevector of 3N simulated qubits.
 *
 * The statevector is arranged as follows:
 *
 * \verbatim
    Q_{2N}    --  Q_{N}     --  Q_{0}  
      |             |            |
    Q_{2N+1}  --  Q_{N+1}   --  Q_{1}
      |             |            | 
    Q_{2N+2}  --  Q_{N+2}   --  Q_{2}
      |             |            |
     ...           ...          ...
      |             |            |
    Q_{3N-1   --   Q_{2N-1} --  Q_{N-1}
   \endverbatim
 *
 * The indices represent the position in the true linear state vector,
 * and the position in the grid show the logical position of each
 * qubit in the cluster state.
 *
 * The rightmost column contains qubits that are measured. One by
 * one, the lowest index qubit is measured out, reducing the size of
 * the state vector by one each time, until the total length of the
 * state vector is 2N. Then qubits are re-allocated on the left hand
 * side (at the high index end of the linear state vector), and
 * re-entangled into the cluster state.
 *
 * 
 */
class ClusterState
{
    using Sim = qsl::Qubits<qsl::Type::Resize, double>;
    const unsigned N; ///< Number of logical qubits 
    unsigned D; ///< The depth of the simulated state (in columns)
    Sim q; ///< The quantum simualtor

public:

    /**
     * \brief Initialise the cluster state object
     *
     * This constructor creates a single column of the cluster state.
     * To add more columns, call the addColumn() function.
     *
     * \param N The number of qubits to include in the column
     * \param entangle A standard vector of bools, length N-1, where
     * entangle[n] is true if there should be a link between the
     * nth and (n+1)th qubit in the column.
     *
     */
    ClusterState(unsigned N, const std::vector<bool> & entangle);
    
    /**
     * \brief Measure out the rightmost column of cluster qubits
     *
     * Call this function to measure and remove the rightmost column
     * of cluster qubits in the bases specified by the the vector prog
     * and the measurement settings s.
     *
     * The measurement outcomes from the column are returned in a 
     * standard vector.
     *
     * \param angles Specifies the measurement bases. The
     * nth element is the basis for the nth qubit (with qubit index
     * increasing down the logical column).
     * \param s The adaptive measurement settings for the current
     * measurement round 
     */
    std::vector<unsigned> measureColumn(std::vector<double> angles,
					std::vector<unsigned> s);

    /**
     * \brief Get the current state
     *
     * Get the current state vector
     *
     */
    qsl::Qubits<qsl::Type::Resize, double> getQubits() const;
    
    /**
     * \brief Re-allocate a column on the left
     *
     * Call this function to introduce a new column of cluster qubits
     * on the left of the rectangle.
     *
     * \param entangle A vector that specifies which vertical links
     * should be present in the new column. If entangle[n] = true,
     * then qubit n and qubit n+1 should be entangled in the column.
     * The vector should have length N-1.
     *
     * All qubits in the new column are entangled horizontally with the
     * column to the right.
     */
    void addColumn(const std::vector<bool> & entangle);
    
};

#endif
