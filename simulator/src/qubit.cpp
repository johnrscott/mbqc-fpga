/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file qubit.cpp
 * \brief Implementation of the Qubit class
 *
 */

#include <fstream>
#include "qubit.hpp"


Measurements LogicalQubit::getMeasurements() const
{
    return m;
}

void LogicalQubit::pushProgram(const Program & prog_in, Entangle entangle_in)
{
    prog.push_back(prog_in);
    entangle.push_back(entangle_in);	
}

void LogicalQubit::addComm(unsigned c_prog)
{
    // If there aren't any programs yet, then skip
    if (prog.size() != 0) {
	prog.back().setc_prog(c_prog);
    }
}

Program LogicalQubit::getProgram(std::size_t n) const
{
    return prog[n];
}

bool LogicalQubit::getEntanglement(std::size_t n) const
{
    return (entangle[n] == Entangle::Below);
}


double LogicalQubit::getAngle(std::size_t n) const
{
    return prog[n].angle();
}

void LogicalQubit::storeOutcome(std::size_t n, const Bits & meas)
{
    // Store the measurement outcome
    m.pushBack(meas(1));

    // Update the byproduct operators
    Bits byp_update = prog[n].bypCorrection(meas);
    byp_current ^= byp_update; 
    byp.push_back(byp_current);

    // Update the next measurement setting
    s_current = prog[n].measurementSetting(m.shiftReg(n,3),
					   byp_stored_current);
    s.push_back(s_current);
}
    
void LogicalQubit::commCorrect(std::size_t n,
		 const Bits & ops_above, const Bits & ops_below)
{
    std::pair<Bits,bool> result = prog[n].commCorrect(ops_above,
						      ops_below);
    // Store the byproduct operators
    if (result.second == true) {
	byp_stored_current = byp_current;
    }
    byp_stored.push_back(byp_stored_current);
	    
    // Add the correction to the byproduct operators
    byp_current ^= result.first;	    
}
    
unsigned LogicalQubit::getSetting() const
{
    return s_current;
}

Bits LogicalQubit::getByproduct() const
{
    return byp_current;
}

Bits LogicalQubit::getByproduct(std::size_t n) const
{
    return byp[n];
}

Bits LogicalQubit::getStoredByproduct(std::size_t n) const
{
    return byp_stored[n];
}

unsigned LogicalQubit::getSetting(std::size_t n) const
{
    return s[n];
}

std::size_t LogicalQubit::size() const
{
    return prog.size();
}
    
void LogicalQubit::print(std::ostream & os) const
{
    if (prog.size() != m.size()) {
	throw std::logic_error("You need to run the MBQC circuit first");
    }
	
    for (std::size_t n = 0; n < prog.size(); n++) {
	os << n;
	os << ", ";
	    
	// Write the program word
	std::ios_base::fmtflags f( os.flags() );
	os << std::hex << std::setw(4) << std::setfill('0')
		  << prog[n].getProgramWord() << " (";
	os.flags( f );
	    	
	os << prog[n] << ")";
	os << ",\t m:" << m(n);
	os << ", s:" << s[n];
	os << ", b:" << byp[n];
	os << ", bs:" << byp_stored[n];
	os << ", a:" << prog[n].angle() << std::endl;
    }
}
