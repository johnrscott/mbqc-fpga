/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file qubit.hpp
 * \brief A class for storing a single logical qubit
 *
 */

#ifndef QUBIT_HPP
#define QUBIT_HPP

#include "measurements.hpp"
#include "program.hpp"

/**
 * \brief A helper class for indicating entanglement structure
 */
enum class Entangle
{
    None,
    Below
};

/**
 * \brief Contains the program for a logical qubit and stores output data
 *
 * This object is associated to a horizontal row in the cluster state. It stores
 * most of the data associated with a logical qubit in the MBQC simulation. It
 * includes the adaptive measurement settings, the byproduct operators and the
 * program for each measurement round. This information is written to a file
 * or printed at the end of the simulation.
 *
 * The implementation of this class is quite close to the hardware implementation
 * to make it easy to compare the two implementations. For a general MBQC 
 * simulator, it would be much better to start from scratch with a fresh program
 * structure that is more generally applicable to all measurement patterns.
 * 
 */
class LogicalQubit
{
    Measurements m; ///< The set of past measurements
    std::vector<Bits> byp; ///< The past byproduct operators 
    std::vector<Bits> byp_stored; ///< The past stored byproduct operators 
    std::vector<unsigned> s; ///< The past measurement settings
    std::vector<Program> prog; ///< Program words
    std::vector<Entangle> entangle; ///< Entanglement to qubit below
 
    unsigned s_current = 0; ///< Current value of s (for use in simulation)
    Bits byp_current{0b00, 2}; ///< Current value of byp (for use in simulation)
    Bits byp_stored_current{0b00, 2}; ///< Current value of stored operators
public:

    /**
     * \brief Construct a logical qubit object with an empty program
     */
    LogicalQubit() = default;

    /** 
     * \brief Append a program word to this logical qubit
     *
     * The first thing to do after constructing the object is to specify the
     * program. You can do this by repeatedly calling this function to 
     * specify the program word for a measurement round, and also specify the
     * entanglement structure for that column.
     *
     * \param prog_in The program word for the measurement round
     * \param entangle_in Specify the entanglement with the qubit below. All
     * logical qubits are responsible for setting the entanglement with the
     * qubit below, not above. 
     */
    void pushProgram(const Program & prog_in, Entangle entangle_in);
    
    /**
     * \brief Add a commutation correction
     *
     * This function is necessary because you don't know whether you need 
     * a commutation correction until the next gate. However, the commutation
     * correction makes a modification to the program of the last column in
     * the previous measurement round.
     */
    void addComm(unsigned c_prog);

    /**
     * \brief Return the set of measurements
     */
    Measurements getMeasurements() const;    

    /**
     * \brief Get the nth program word
     */
    Program getProgram(std::size_t n) const;
    
    /**
     * \brief Get the nth entanglement
     *
     * The only options are None and Below. Entanglement to the qubit above
     * is handled by the qubit above. The functions returns true if there 
     * is entanglement with the qubit below, and false otherwise.
     */
    bool getEntanglement(std::size_t n) const;

    /**
     * \brief Get the nth program angle
     *
     */
    double getAngle(std::size_t n) const;
    
    /**
     * \brief Store a measurement outcome
     *
     * Store a measurement outcome for the current logical qubit. When 
     * a measurement outcome is stored, the byproduct operator is also
     * updated for that qubit, and the next measurement setting is
     * computed.
     *
     * The outcome from the qubits above and below are necessary for 
     * the byproduct operator calculation. If there is no qubit above
     * or below, pass zero. These are passed in the vector meas,
     * where meas[2] is the outcome above, meas[1] is the current outcome,
     * and meas[0] is the outcome below.
     * 
     */
    void storeOutcome(std::size_t n, const Bits & meas);
    
    /**
     * \brief Calculate the commutation correction
     *
     */
    void commCorrect(std::size_t n,
		     const Bits & ops_above, const Bits & ops_below);
    
    /**
     * \brief Get the latest measurement setting
     *
     * This function returns the most recently computed
     * measurement setting.
     */
    unsigned getSetting() const;
    
    /**
     * \brief Get the current byproduct operators
     */
    Bits getByproduct() const;
    
    /**
     * \brief Get the byproduct operator at measurement round n
     */
    Bits getByproduct(std::size_t n) const;
    
    /**
     * \brief Get the stored byproduct operator at measurement round n
     */
    Bits getStoredByproduct(std::size_t n) const;
    
    /**
     * \brief Get the adaptive measurment setting at round n
     */
    unsigned getSetting(std::size_t n) const;
    
    /**
     * \brief Get the size of the program
     */
    std::size_t size() const;
    
    /// Print the history of the logical qubit to ostream os
    void print(std::ostream & os) const;
    
};

#endif
