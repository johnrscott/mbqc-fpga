/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file mbqc.cpp
 * \brief Implementation of MBQC simulator
 *
 */

#include <iostream>
#include "cluster.hpp"
#include "qubit.hpp"
#include "mbqc.hpp"
#include <fstream>

#define X_BASIS 0
#define Y_BASIS (M_PI/2)

void MBQC::addUnitary(unsigned targ, double xi, double eta, double zeta)
{
    // Add the instruction to store the byproduct operators
    addComm(targ, 0b00001);  
    
    pushProgram(targ, {0,     0b01100, 0b000010}, Entangle::None);
    pushProgram(targ, {-xi,   0b10100, 0b010000}, Entangle::None);
    pushProgram(targ, {-eta,  0b01101, 0b000010}, Entangle::None);
    pushProgram(targ, {-zeta, 0b00000, 0b010000}, Entangle::None);
}

void MBQC::addIdentity(unsigned targ)
{
    pushProgram(targ, {0, 0b00000, 0b000010}, Entangle::None);
    pushProgram(targ, {0, 0b00000, 0b010000}, Entangle::None);
}

void MBQC::addHadamard(unsigned targ)
{
    pushProgram(targ, {X_BASIS, 0b00000, 0b010000}, Entangle::None);
    pushProgram(targ, {Y_BASIS, 0b00000, 0b000010}, Entangle::None);
    pushProgram(targ, {Y_BASIS, 0b00000, 0b010000}, Entangle::None);
    pushProgram(targ, {Y_BASIS, 0b00000, 0b010010}, Entangle::None);
}

void MBQC::addControlNot(unsigned ctrl, unsigned targ)
{
    // Check that the control and target and neighbouring
    if (ctrl + 1 != targ and ctrl - 1 != targ) {
	throw std::logic_error("Cannot perform the cnot gate between "
			       "non-neighbouring qubits "
			       + std::to_string(ctrl) + " and "
			       + std::to_string(targ));
    }
	    
    // Add the commutation correction to last program word
    addComm(ctrl, 0b00110);
    addComm(targ, 0b01010);
    
    // Control qubit
    pushProgram(ctrl, {X_BASIS, 0b00000, 0b000011}, Entangle::None); // 0
    pushProgram(ctrl, {Y_BASIS, 0b00000, 0b010000}, Entangle::None); // 1
    pushProgram(ctrl, {Y_BASIS, 0b00000, 0b010011,
		       0b10100}, Entangle::None); // 2
    pushProgram(ctrl, {X_BASIS, 0b00000, 0b000010}, Entangle::Below); // 3
    pushProgram(ctrl, {Y_BASIS, 0b00000, 0b010010}, Entangle::None); // 4
    pushProgram(ctrl, {Y_BASIS, 0b00000, 0b010000}, Entangle::None); // 5

    // Target qubit
    pushProgram(targ, {X_BASIS, 0b00000, 0b000010}, Entangle::None); // 7
    pushProgram(targ, {X_BASIS, 0b00000, 0b110000}, Entangle::None); // 8
    pushProgram(targ, {X_BASIS, 0b00000, 0b100010}, Entangle::None); // 9
    pushProgram(targ, {X_BASIS, 0b00000, 0b010000}, Entangle::None); // 10
    pushProgram(targ, {X_BASIS, 0b00000, 0b000010}, Entangle::None); // 11
    pushProgram(targ, {X_BASIS, 0b00000, 0b010000}, Entangle::None); // 12
}

void MBQC::pushProgram(std::size_t n, const Program & prog, Entangle entangle)
{
    // Check that n is in range
    if (n >= qubits.size()) {
	throw std::out_of_range("n is out of range in pushProgram");
    }
    qubits[n].pushProgram(prog, entangle);
}

void MBQC::addComm(std::size_t n, unsigned comm)
{
    qubits[n].addComm(comm);
}
    

MBQC::MBQC(unsigned N)
    : N{N}, cluster{N, std::vector<bool>(N-1,false)},
      qubits(N)
{
}

/// Return the number of logical qubits
unsigned MBQC::getNumQubits() const
{
    return N;
}

void MBQC::print(unsigned n, std::ostream & os) {
    os << "Logical Qubit " << n << std::endl;
    qubits[n].print(os);
}

void MBQC::writeToFile(unsigned n, std::string filename)
{
    // Create and open a text file
    std::ofstream file{filename};

    // Write the header row
    file << "line,reset,enable,c_prog,s_prog,b_prog,meas,"
	 << "ops_above,ops_below,ops_stored,ops_current,s"
	 << std::endl;

    // Write the data
    for (std::size_t k = 0; k < qubits[n].size(); k++) {

	// Get the kth program
	Program prog{ qubits[n].getProgram(k) };

	file << k << ","; // Write the line number
	file << "0,1,"; // Write reset = 0, enable = 1;
	file << prog.c_prog() << ","; // Commutation correction
	file << prog.s_prog() << ","; // Adaptive measurement
	file << prog.b_prog() << ","; // Byproduct computation

	// Get the measurements from the qubits above and below
	Bits meas; // Make an empty bitstring object
		
	// Include the measurment from above
	if (n < qubits.size()-1) {
	    meas.pushBit(qubits[n+1].getMeasurements()(k));
	} else {
	    meas.pushBit(0);
	}
		
	// Always include the current qubit measurement
	meas.pushBit(qubits[n].getMeasurements()(k));

	// Include the measurment from above
	if (n > 0) {
	    meas.pushBit(qubits[n-1].getMeasurements()(k));
	} else {
	    meas.pushBit(0);
	}

	file << meas << ","; // Measurement
	    
		
	// Get the byproduct operators
	Bits ops_above{0b00, 2};
	Bits ops_current = qubits[n].getByproduct(k);
	Bits ops_below{0b00, 2};
	if (n > 0) {
	    ops_above = qubits[n-1].getByproduct(k);
	}
	if (n < qubits.size()-1) {
	    ops_below = qubits[n+1].getByproduct(k);
	}

	// Since ops_stored is calculated on clk_r and not used until
	// the clk_s of the next cycle, it must be offset by one in
	// the table in order to be interpreted correctly by the
	// testbench
	Bits ops_stored{ 0b00, 2 };
	if (k > 0) {
	    ops_stored = qubits[n].getStoredByproduct(k-1);
	}

	file << ops_above << ",";
	file << ops_below << ",";
	file << ops_stored << ",";
	file << ops_current << ",";
	file << qubits[n].getSetting(k);
	file << std::endl;
    }

	    
    // Close the file
    file.close();
}

void MBQC::writeToFile(const std::string & filename)
{
    // Create and open a text file
    std::ofstream file{filename};

    // Write the header row
    file << "line,meas,ops,s" << std::endl;

    // Write the data
    for (std::size_t k = 0; k < qubits[0].size(); k++) {

	file << k << ","; // Write the line number

	// Get the measurements from all the qubits objects
	Bits meas;
	for (std::size_t n = 0; n < N; n++) {
	    meas.pushBit(qubits[n].getMeasurements()(k));
	}
	file << meas << ","; // Measurement
	    
		
	// Get the byproduct operators
	Bits ops;
	for (std::size_t n = 0; n < N; n++) {
	    ops = qubits[n].getByproduct(k) + ops;
	}
	file << ops << ","; // Byproduct ops

	// Get the measurement settings
	Bits s;
	for (std::size_t n = 0; n < N; n++) {
	    s.pushBit(qubits[n].getSetting(k));
	}
	file << s << std::endl; // Adaptive measurement setting
    }
	    
    // Close the file
    file.close();
}

void MBQC::writeReadableFile(unsigned n, std::string filename)
{
    // Create and open a text file
    std::ofstream file{filename};

    // Write the header row
    file << "Address,c_prog,s_prog,b_prog,meas,"
	 << "ops_above,ops_below,ops_stored,ops_current,s,P"
	 << std::endl;

    // Write the data
    for (std::size_t k = 0; k < qubits[n].size(); k++) {

	// Get the kth program
	Program prog{ qubits[n].getProgram(k) };

	file << k << ","; // Write the line number
	file << prog.c_prog() << ","; // Commutation correction
	file << prog.s_prog() << ","; // Adaptive measurement
	file << prog.b_prog() << ","; // Byproduct computation
		
	// Get the measurements from the qubits above and below
	Bits meas; // Make an empty bitstring object
		
	// Include the measurment from above
	if (n < qubits.size()-1) {
	    meas.pushBit(qubits[n+1].getMeasurements()(k));
	} else {
	    meas.pushBit(0);
	}
		
	// Always include the current qubit measurement
	meas.pushBit(qubits[n].getMeasurements()(k));

	// Include the measurment from above
	if (n > 0) {
	    meas.pushBit(qubits[n-1].getMeasurements()(k));
	} else {
	    meas.pushBit(0);
	}

	file << meas << ","; // Measurement
	    
		
	// Get the byproduct operators
	Bits ops_above{0b00, 2};
	Bits ops_current = qubits[n].getByproduct(k);
	Bits ops_below{0b00, 2};
	if (n > 0) {
	    ops_above = qubits[n-1].getByproduct(k);
	}
	if (n < qubits.size()-1) {
	    ops_below = qubits[n+1].getByproduct(k);
	}

	// Since ops_stored is calculated on clk_r and not used until
	// the clk_s of the next cycle, it must be offset by one in
	// the table in order to be interpreted correctly by the
	// testbench
	Bits ops_stored{ 0b00, 2 };
	if (k > 0) {
	    ops_stored = qubits[n].getStoredByproduct(k-1);
	}

	file << ops_above << ",";
	file << ops_below << ",";
	file << ops_stored << ",";
	file << ops_current << ",";
	file << qubits[n].getSetting(k) << ",";

	file << std::hex << std::setw(4) << std::setfill('0')
	     << prog.getProgramWord();
	file << std::endl;


    }

    // Close the file
    file.close();

}

void MBQC::writeTableFile(std::string filename)
{
    // Create and open a text file
    std::ofstream file{filename};

    // Write the header row
    file << "Address "; 
    for (std::size_t n = 0; n < N; n++) {
	file << "& $m_{" << n <<"} & P_{" << n << "} & \\theta_{"
	     << n << "} & s_{" << n << "} & b_{" << n << "}";
    }

    // Remember you need to escape the backslashes
    file << "\\\\" << std::endl;

    // Write the data
    // \todo Fix the size checking
    for (std::size_t k = 0; k < qubits[0].size(); k++) {

	// Write the address
	file << k << " & ";
		
	// Loop over each logical qubit
	for (std::size_t n = 0; n < N; n++) {

	    if (n != 0) {
		file << " & ";
	    }
		    
	    // Write the measurement outcome
	    file << qubits[n].getMeasurements()(k) << " & ";

	    // Get the kth program
	    Program prog{ qubits[n].getProgram(k) };
		    
	    // Write the program word
	    std::ios_base::fmtflags f( file.flags() );
	    file << std::hex << std::setw(4) << std::setfill('0')
		 << prog.getProgramWord() << " & ";
	    file.flags( f );

	    // Write the angle value
	    file << prog.angle() << " & ";

	    // Write the adaptive measurement setting
	    file << qubits[n].getSetting(k) << " & ";

	    // Write the byproduct operators
	    Bits ops_current = qubits[n].getByproduct(k);
	    file << ops_current;

	}
	file << " \\\\" << std::endl;
		
    }
	    
    // Close the file
    file.close();
}
    
void MBQC::writeProgToFile(unsigned n, const std::string & filename)
{
    // Create and open a text file
    std::ofstream file{filename};

    // Write the header row
    file << "memory_initialization_radix=2;" << std::endl;
    file << "memory_initialization_vector=";

    // Write the data
    for (std::size_t k = 0; k < qubits[n].size(); k++) {

	// Get the kth program
	Program prog{ qubits[n].getProgram(k) };
		
	file << prog.c_prog(); // Commutation correction
	file << prog.s_prog(); // Adaptive measurement
	file << prog.b_prog(); // Byproduct computation
	file << std::endl; // Write newline separator
	    
    }
	    
    // Close the file
    file.close();
}

/// Write the program for all qubits to a file
void MBQC::writeCoefficients(const std::string & filename)
{
    // Create and open a text file
    std::ofstream file{filename};

    // Write the header row
    file << "memory_initialization_radix=16;" << std::endl;
    file << "memory_initialization_vector=";

    // Loop over the program counter
    for (std::size_t k = 0; k < qubits[0].size(); k++) {

	// Loop over the qubits
	// The qubit program words must be printed in reverse
	// order for the coefficients file so that the program
	// word for qubit 0 is the least significant word
	for (int n = N-1; n >= 0; n--) {
		    
	    // Get the kth program
	    Program prog{ qubits[n].getProgram(k) };

	    // Write the program word
	    std::ios_base::fmtflags f( file.flags() );
	    file << std::hex << std::setw(4) << std::setfill('0')
		 << prog.getProgramWord();
	    file.flags( f );
	}
		
	file << std::endl; // Write newline separator
    }
		
    // Close the file
    file.close();
}

    
qsl::Qubits<qsl::Type::Resize, double> MBQC::run()
{
    // Print info
    std::cout << std::endl << "Started simulation" << std::endl;
	
    // First equalise the length of all the qubits
    std::size_t max_len = 0;
    for (std::size_t n = 0; n < qubits.size(); n++) {
	max_len = std::max(max_len, qubits[n].size());
    }

    // Now pad all the qubits to the same length
    for (std::size_t n = 0; n < qubits.size(); n++) {
	while(qubits[n].size() < max_len) {
	    addIdentity(n);
	}
    }
	
    // Check that everything is the right length
    std::size_t length = qubits[0].size(); ///< Program length
    for (std::size_t n = 1; n < qubits.size(); n++) {
	if (qubits[n].size() != length) {
	    throw std::runtime_error("All the qubits must have the same "
				     "length before run() is called.");
	}
    }

    for (std::size_t k = 0; k < length; k++) {	    
	    
	// The first column has already been added in the constructor
	// of the cluster state. The column that is added here must have
	// the correct entanglement structure for the next measurement
	// round, hence the index is out by one. 
	std::vector<bool> entangle;
	for (std::size_t n = 0; n < qubits.size() - 1; n++) {
	    if (k+1 < length) {
		// If not at the final measurement round,
		// then add the entanglement structure for the
		// next measurement round.
		entangle.push_back(qubits[n].getEntanglement(k+1));
	    } else {
		// If this is the last measurement round,
		// then the column currently being added is the
		// final state of the circuit, which should not
		// be entangled for any of the measurement
		// patterns implemented
		entangle.push_back(false);
	    }
	}
	    
	cluster.addColumn(entangle);

	// Generate the measurement settings for the column
	std::vector<double> angles;
	std::vector<unsigned> settings;
	for (std::size_t n = 0; n < qubits.size(); n++) {
	    angles.push_back(qubits[n].getAngle(k));
	    settings.push_back(qubits[n].getSetting());
	}

	// Measure the cluster qubit in the correct basis
	std::vector<unsigned> outcomes
	    = cluster.measureColumn(angles, settings);
	    
	// Store the outcome. This internally updates the byproduct
	// operators and measurement settings of each logical qubit
	for (std::size_t n = 0; n < qubits.size(); n++) {
		
	    // Make a vector of the outcomes above and below the
	    // current logical qubit
	    Bits meas{0b000, 3};

	    // If the current logical qubit has a neighbour
	    // below it, then include that outcome
	    if (n < qubits.size() - 1) {
		meas(0) = outcomes[n+1];
	    }

	    // Always include the outcome for the current qubit
	    meas(1) = outcomes[n];
		
	    // If the current logical qubit has a neighbour
	    // above it, then include that measurement outcome
	    if (n > 0) {
		meas(2) = outcomes[n-1];
	    }
		
	    qubits[n].storeOutcome(k, meas);

	    // Update the byproduct operators using the
	    // comm part of the program
	    Bits ops_above{0b00, 2};
	    Bits ops_below{0b00, 2};
	    if (n > 0) {
		ops_above = qubits[n-1].getByproduct();
	    }
	    if (n < qubits.size()-1) {
		ops_below = qubits[n+1].getByproduct();
	    }

	    qubits[n].commCorrect(k, ops_above, ops_below);
	}

	std::cout << "Finished round " << k << std::endl;
    }

    // Print the byproduct operators
    std::cout << "Run finished" << std::endl << std::endl;
    std::cout << "Final byproduct operators" << std::endl;
    for (std::size_t n = 0; n < qubits.size(); n++) {
    }
	
    // Perform the byproduct operator corrections
    auto q = cluster.getQubits();
    for (std::size_t n = 0; n < qubits.size(); n++) {
	const Bits byp = qubits[n].getByproduct();
	std::cout << "Logical qubit " << n
		  << ": x = " << byp(1)
		  << ", z = " << byp(0) << std::endl;
	if (byp(1) == 1) {
	    q.pauliX(n);
	}
	if (byp(0) == 1) {
	    q.pauliZ(n);
	}
    }
    
    return q;	
}

void MBQC::unitary(unsigned targ, double xi, double eta, double zeta)
{
    // There is no need to check the length of the other qubit
    // programs here, just push back the unitary
    addUnitary(targ, xi, eta, zeta);
}

void MBQC::hadamard(unsigned targ)
{
    // There is no need to check the length of the other qubit
    // programs here, just push back the unitary
    addHadamard(targ);
}

void MBQC::controlNot(unsigned ctrl, unsigned targ)
{
    // Check that both the ctrl and targ qubits have programs
    // of equal length. If not, push back identities to make
    // the lengths equal
    const std::size_t ctrl_len{ qubits[ctrl].size() };
    const std::size_t targ_len{ qubits[targ].size() };
    if (ctrl_len != targ_len) {
	// First check that the lengths differ by a multiple
	// of two, so they can be corrected by identity gates
	if (((ctrl_len - targ_len) % 2) == 0) {
	    // Pad the shorter qubit with identities
	    if (ctrl_len < targ_len) {
		while (qubits[ctrl].size() < targ_len) {
		    addIdentity(ctrl);
		}
	    } else {
		while (qubits[targ].size() < ctrl_len) {
		    addIdentity(targ);
		}
	    }
	} else {
	    throw std::runtime_error("Unable to correct using identity");
	}
    }

    // Now add the CNOT gate
    addControlNot(ctrl,targ);
}
