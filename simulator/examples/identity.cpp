/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file identity.cpp
 * \brief Example identity gate using MBQC
 *
 */

#include <qsl/qubits.hpp>
#include <qsl/utils/quantum.hpp>

#include <iostream>

int main()
{
    using Sim = qsl::Qubits<qsl::Type::Resize, double>;
    
    // 
    Sim q{ 3 };

    // Make equal superposition state
    for (unsigned k = 0; k < 3; k++) {
	q.hadamard(k);
    }

    // Make the cluster state
    q.controlPhase(0,1,M_PI); // CZ
    q.controlPhase(1,2,M_PI); // CZ
    q.print();

    // Measure qubit 0 in X basis and remove
    double phi = 0; // Set X basis
    q.phase(0, -phi-M_PI/2); // Z-rotation 
    q.rotateX(0, -M_PI/2); // X-rotation 
    const unsigned z = q.measureOut(0);
    std::cout << "z = " << z << std::endl;
    q.print();

    // Measure qubit 1 in X basis and remove
    phi = 0; // Set X basis
    q.phase(0, -phi-M_PI/2); // Z-rotation 
    q.rotateX(0, -M_PI/2); // X-rotation 
    const unsigned x = q.measureOut(0);
    std::cout << "x = " << x << std::endl;

    // The final state should be equal to the |+) state up
    // to the byproduct operators
    Sim q_correct{ 1 };
    q_correct.hadamard(0); // Create the plus state

    // Use the byproduct operators to correct the state
    if (x == 1) {
	q.pauliX(0);
    }
    if (z == 1) {
	q.phase(0, M_PI);
    }
    q.print();

    // Check the distance between states
    std::cout << "Distance = " << qsl::fubiniStudy(q.getState(),
						   q_correct.getState())
	      << std::endl;
}
