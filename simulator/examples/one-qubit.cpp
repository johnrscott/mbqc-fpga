/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file one-qubit.cpp
 * \brief Example one-qubit gate using MBQC
 *
 */

#include <qsl/qubits.hpp>
#include <qsl/utils/quantum.hpp>
#include <qsl/utils/random.hpp>

#include <iostream>

int main()
{
    using Sim = qsl::Qubits<qsl::Type::Resize, double>;
    
    Sim q{ 5 };

    ///\todo Change phase to rotateZ to avoid sign problem
    
    // Make equal superposition state
    for (unsigned k = 0; k < 5; k++) {
	q.hadamard(k);
    }

    // Generate random phases
    qsl::Random<double> rand(0,2*M_PI);
    const double xi = rand.getNum();
    const double eta = rand.getNum();
    const double zeta = rand.getNum();

    // Byproduct operators
    unsigned z = 0;
    unsigned x = 0;

    // Measurement setting
    unsigned s = 0;
    
    // Make the cluster state
    q.controlPhase(0,1,M_PI); // CZ
    q.controlPhase(1,2,M_PI); // CZ
    q.controlPhase(2,3,M_PI); // CZ
    q.controlPhase(3,4,M_PI); // CZ

    // Measure qubit 0 in X basis and remove
    double phi = 0; // Set X basis
    q.phase(0, phi+M_PI/2); // Z-rotation 
    q.rotateX(0, -M_PI/2); // X-rotation 
    const unsigned out0 = q.measureOut(0);
    z ^= out0;
    std::cout << "z = " << z << std::endl;

    // Measure qubit 1 in xi basis and remove
    s = out0;
    if (s == 0) {
	phi = xi;
    } else {
	phi = -xi;
    }
    q.phase(0, phi+M_PI/2); // Z-rotation 
    q.rotateX(0, -M_PI/2); // X-rotation 
    const unsigned out1 = q.measureOut(0);
    x ^= out1;
    std::cout << "x = " << x << std::endl;

    // Measure qubit 2 in eta basis and remove
    s = out1;
    if (s == 0) {
	phi = eta;
    } else {
	phi = -eta;
    }
    q.phase(0, phi+M_PI/2); // Z-rotation 
    q.rotateX(0, -M_PI/2); // X-rotation 
    const unsigned out2 = q.measureOut(0);
    z ^= out2;
    std::cout << "z = " << z << std::endl;

    // Measure qubit 3 in zeta basis and remove
    s = out2 ^ out0;
    if (s == 0) {
	phi = zeta;
    } else {
	phi = -zeta;
    }
    q.phase(0, phi+M_PI/2); // Z-rotation 
    q.rotateX(0, -M_PI/2); // X-rotation 
    const unsigned out3 = q.measureOut(0);
    x ^= out3;
    std::cout << "x = " << x << std::endl;
    
    // The final state should be equal to the |+) state up
    // to the byproduct operators
    Sim q_correct{ 1 };
    q_correct.hadamard(0); // Create the plus state

    // Apply the rotations - U = Rx(\zeta)Rz(\eta)Rx(\xi)
    q_correct.rotateX(0, xi);
    q_correct.phase(0, -eta);
    q_correct.rotateX(0, zeta);
    
    // Use the byproduct operators to correct the state
    if (x == 1) {
	q.pauliX(0);
    }
    if (z == 1) {
	q.phase(0, M_PI);
    }
    q.print();

    // Check the distance between states
    std::cout << "Distance = " << qsl::fubiniStudy(q.getState(),
						   q_correct.getState())
	      << std::endl;
}
