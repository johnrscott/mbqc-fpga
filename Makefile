# Build the documentation
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = docs
BUILDDIR      = docs
.PHONY: docs
docs:
	doxygen docs/Doxyfile-vhd
	doxygen docs/Doxyfile-sim
	rm -rf docs/html
	@$(SPHINXBUILD) -M html "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	mv docs/vhdldocs docs/html/vhdldocs
	mv docs/simdocs docs/html/simdocs


.PHONY: clean
clean:
	-rm -rf $(SOURCEDIR)/xml $(SOURCEDIR)/doxygen-html $(SOURCEDIR)/html $(SOURCEDIR)/doctrees
