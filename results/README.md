# Static timing analysis 

The intent of this analysis is to discover the maximum viable clock frequency for the design, and establish the input/output delay requirements at each clock frequency.

## Method

- First establish the maximum operating frequency of the design, by manually adjusting the phase of clk\_s and clk\_r to balance the worst slack between the path from the measurment input to the clk\_s sample, and the path from the clk\_s sample to the output adaptive measurement setting s. Use (unrealistic) constraints that leave zero t\_co on the measurement end, and zero setup time for s on the output end. These conditions will allow the static timing analysis to pass at the largest possible frequency. The choice of phase shift of clk\_s amounts to balancing the output delay between the input and output critical paths.

- After the largest operating frequency F\_max has been found, fix the the phase settings used to obtain it. Then run the implementation at each frequency between 10MHz and F\_max, in steps of 10MHz. For each implementation, check that the static timing analysis passes. Read the setup slack for the input/output paths of interest, and add slightly tighter constraints to try force the place-and-route optimisation to do better. If it does, repeat the process to obtain the tightest input/output constraints that still work. Stop when the placer-and-router cannot do better. Then take the constraints and slack to determine the maximum valid input delay for the measurement input, and the setup time for the adaptive measurement setting at the output.

- For each frequency, record the following information about the timed paths: the timing slack for the input path; the timing slack for the output path; the available time accounting for the constraints; the division of each timing path into routing delay, logic delay, etc. Include the IBUF and OBUF propagation delays.

- After obtaining the timing analysis data for each frequency, plot the following graphs

## Notes

The input critical path can be found by looking at the timing report for "clk\_p\_in to clk\_s\_mbqc1\_clk\_wiz\_0\_0. The worst negative setup slack is used to provide a slightly tighter (by 1ps) constraint on the t\_co\_max of the meas input, which forces the place-and-route optimiser to try to do better. 

The output critical path can be found by looking for the clk\_s\_mbqc1\_clk\_wiz\_0\_0 to clk\_p\_in path. The negative slack on the path to the output s is used to forma a slightly stronger (by 1ps) constraint on the s\_setup parameter, to force the place-and-route optimiser to try to do better.

Note that the path to the byproduct operators is slightly faster than the path to the output s because of the lack of the LUT (for the combinational logic) in the byproduct operator path.

If the place-and-route optimiser cannot do better, then the critical paths should fail with 1ps negative slack. This is taken as the basis for the timing analysis. If the place-and-route optimiser produces a better design, iterate the constraint update until the timing fails.

All the numbers quoted in the timing analysis are in nanoseconds.

For each timing analysis, the timing report is contained in the file `timing_*MHz.rpx`. You can open it by running `open_report -name timing "results/timing_*MHz.rpx"` n the TCL console.

## Results

The maximum frequency obtained was 190MHz, using clk\_s phase 220deg and clk\_r phase 300deg.

The following give the detailed timing results for each frequency from 190MHz down to 10MHz.

### 190MHz (5.26ns)

Number of constraint update iterations: 2

- Output path (path 65):
  - output delay: 0.060 (1.1%)
  - slack: -0.001
  - data route: 1.598
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 0.139 (2.64%)
  - slack: -0.001
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086
  
- Process time: 96.26%

### 180MHz (5.56ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 0.185 (3.3%)
  - slack: -0.016
  - data route: 1.598
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 0.320 (5.76%)
  - slack: -0.009
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

- Process time: 90.94%

### 170MHz (5.88ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 0.304 (5.17%)
  - slack: -0.017
  - data route: 1.598
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 0.526 (8.95%)
  - slack: -0.001
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

- Process time: 85.88%

### 160MHz (6.25ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 0.450 (7.2%)
  - slack: 0.000
  - data route: 1.573
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 0.745 (11.92%)
  - slack: -0.002
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

- Process time: 80.88%

### 150MHz (6.67ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 0.580 (8.70%)
  - slack: -0.001
  - data route: 1.598
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 1.012 (15.17%)
  - slack: 0.000
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

Process time: 76.13%

### 140MHz (7.14ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 0.770 (10.78%)
  - slack: -0.013
  - data route: 1.598
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 1.302 (18.24%)
  - slack: -0.006
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086
  
- Process time: 70.98%

### 130MHz (7.69ns)

Number of constraint update iterations: 2

- Output path (path 65):
  - output delay: 0.956 (12.43%)
  - slack: 0.000
  - data route: 1.540
  - FDRE: 0.204
  - LUT5: 0.126
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 1.630 (21.20%)
  - slack: -0.007
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

- Process time: 66.37%

### 120MHz (8.33ns)

Number of constraint update iterations: 2

- Output path (path 65):
  - output delay: 1.234 (14.8%)
  - slack: -0.003
  - data route: 1.569
  - FDRE: 0.233
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 2.066 (24.80%)
  - slack: -0.069
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086
  
- Process time: 60.40%

### 110MHz (9.09ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 1.522
  - slack: -0.037
  - data route: 1.598
  - FDRE: 0.233
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 2.460
  - slack: -0.004
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

### 100MHz (10.00ns)

Number of constraint update iterations: 4

- Output path (path 65):
  - output delay: 1.865
  - slack: -0.041
  - data route: 1.598
  - FDRE: 0.233
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 2.999
  - slack: -0.001
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

### 90MHz (11.11ns)

Number of constraint update iterations: 7

- Output path (path 65):
  - output delay: 2.275
  - slack: -0.008
  - data route: 1.506
  - FDRE: 0.204
  - LUT5: 0.126
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 3.660
  - slack: -0.001
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

### 80MHz (12.50ns)

Number of constraint update iterations: 2

- Output path (path 65):
  - output delay: 2.738
  - slack: -0.042
  - data route: 1.595
  - FDRE: 0.204
  - LUT5: 0.126
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 4.487
  - slack: -0.001
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

### 70MHz (14.29ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 3.319
  - slack: -0.021
  - data route: 1.598
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 5.435
  - slack: -0.011
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

### 60MHz (16.67ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 3.791
  - slack: -0.065
  - data route: 1.578
  - FDRE: 0.204
  - LUT5: 0.126
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 6.799
  - slack: -0.008
  - data route: 0.503
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086


### 50MHz (20.00ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 5.340
  - slack: -0.041
  - data route: 1.598
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 8.710
  - slack: -0.005
  - data route: 0.492
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

### 40MHz (25.00ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 7.282
  - slack: -0.027
  - data route: 1.598
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 11.775
  - slack: -0.003
  - data route: 0.492
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

### 30MHz (33.33ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 10.352
  - slack: 0.000
  - data route: 1.572
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 16.695
  - slack: -0.002
  - data route: 0.494
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

### 20MHz (50.00ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 16.630
  - slack: 0.000
  - data route: 1.574
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 26.678
  - slack: -0.001
  - data route: 0.494
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

### 10MHz (100.00ns)

Number of constraint update iterations: 3

- Output path (path 65):
  - output delay: 35.070
  - slack: -0.020
  - data route: 1.5849
  - FDRE: 0.223
  - LUT5: 0.043
  - OBUF: 1.883

- Input path (path 29)
  - input delay: 56.160
  - slack: -0.026
  - data route: 0.492
  - IBUF: 0.716
  - LDCE: 0.341
  - LUT3: 0.086

# Utilisation

LUTs: 11/41000
Slice registers: 24/82000
Slice: 8 (10250)
Bonded IOB: 8/285
ILOGIC: 1
BUFGCTRL: 3

