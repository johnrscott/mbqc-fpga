# Static timing analysis for Ultrascale+

This document contains the method for measuring the timing performance of the ultrascale+ implementation.

## Measuring max input/output delays at each clock frequency

### Method

- First establish the maximum operating frequency of the design. Similarly to the 7-series, the maximum frequency is obtained by adjusting the phases of clk\_s and clk\_r while increasing the frequency, until the two critical paths have no remaining slack. First, assuming that the data propagation delay along the input path is constant with frequency (it is the sum of IBUF, LDCE propagation and LUT3 propagation), clk\_s can be adjusted until the clock arrival time coincides with the data arrival time at the FDRE element (ops\_reg). This is a function of both the frequency and phase of clk\_s. The maximum frequency is obtained when the clk\_s phase is reduced as much as possible but the output critical path s still fails.

### Notes

- In the ultrascale+ architecture, it is not possible to bring the LDCE into the IOB region (because that is not a feature of the architecture -- compare the SelectIO resources manuals for 7-series and ultrascale). However, the delays of each of the elements is smaller in the ultrascale+ device, meaning that there is a slight timing improvement in the input path despite the presence of input buffers and routing.

- The highest speed obtainable using HSTL and the LDCE latch is 220MHz (fails at 230MHz, worst negative slack -0.176ns). The phases at this frequency are 120deg (clk\_s) and 230deg (clk\_r). The breakdown of the input critical path at 230MHz is

	* INBUF: 0.423ns
	* routing: 0.791ns
	* LDCE: 0.067ns
	* LUT3: 0.047ns

When testing with the FDPE flop instead of the latch, it is necessary to add a command `config_timing_analysis -enable_preset_clear_arcs true` to treat the PRE -> D path through the FDPE element as a combinational path. (What happens without the command is that the meas -> PRE path is timed for setup with respect to clk\_r, which is wrong, and the output from the FDPE is timed from the clk\_r to clk\_s domains, which is also wrong).

The breakdown of the critical path for the FDPE case at 230MHz is 

	* INBUF: 0.423ns
	* routing: 0.843ns
	* FDPE: 0.154ns
	* LUT3: 0.089ns
	
Hence, the latch based method is faster. 

### Results

## Measuring max clock frequency for each number of qubits

The purpose of this test is to see if the maximum clock frequency depends on the number of qubits to be implemented. It uses the block design mbqc2, which is made from a multi\_qubit object that has a generic which specifies the number of qubits to implement. The memory is stored in a distributed (ROM) memory generator as a flat program, with the program word for each qubit concaternated to form a wide memory bus. The proggen program generates the coefficient file for this memory module. It is necessary to include the memory to prevent Vivado optimising away the logic.

It is expected that the clock frequency will decrease with increased qubits due to placement and routing issues. 

### Method

The maximum clock frequency was investigated for each number of qubits from 8 to 64, in multiples of 8. The limit 64 was chosen because it uses the maximum width of the memory generator output bus, which is not a hard limit. However, the results will be enough to show whether the clock frequency depends on implementation size.

At each (ascending) number of qubits N, the block design mbqc2 was modified for the given number of ubits, and a memory coefficient file was generated using `proggen` for N qubits and stored in the distributed memory generator (the block design was reset and regenereted to make sure the file is loaded properly). Then the design is implemented at the optimum clock frequency of the previous number of qubits, using the same values of clock phases as in the input/output timing analysis. Depending on the worst negative slack, the clock frequency is adjusted so as to obtain an approximation to the largest operational frequency of the device, using interval bisection, stopping when the interval is less than 5MHz.

### Results

#### 
