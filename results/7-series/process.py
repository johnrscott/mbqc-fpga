# Compute the percentage of time split between the input, output and
# digital process. Frequencies are in MHz and times are in ns

import pandas as pd

# Read the data file
df = pd.read_csv("timing.dat", delim_whitespace = True)

# Compute the true input and output delay by adding the slack
df["in_delay_true"] = df["in_delay"] + df["in_slack"]
df["out_delay_true"] = df["out_delay"] + df["out_slack"]

# Compute the period, and derive the resulting processing time
df["period"] = 1/df["freq"]
df["process_time"] = df["period"] - df["in_delay_true"] - df["out_delay_true"] 

# Make empty dictionary for process times
data = {}

# Copy the frequency data
data["freq"] = df["freq"]/1e6 # Convert to MHz for plotting
data["freq"] = data["freq"].map(lambda x: int(x))

# Write input and output delay as a percentage of period
data["input"] = 100*df["in_delay_true"]/df["period"]
data["process"] = 100*df["process_time"]/df["period"]
data["output"] = 100*df["out_delay_true"]/df["period"]

# Make dataframe from dictionary
out_df = pd.DataFrame(data)

print(out_df)
print(df)

# Write the dataframe to a file
out_df.to_csv("process.dat", sep=" ", index = False, float_format = "%.2f")
